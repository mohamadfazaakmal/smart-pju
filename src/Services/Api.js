/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/rules-of-hooks */
import * as React from 'react';
import { Config } from '../Config';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { pingSelector } from '../Store/Actions/Ping';

//console.log('setup: ', setupUrl._W);
const baseQeury = Config.API_URL;
export const Api = () => {
    const url = useSelector(pingSelector);
    return axios.create({
        baseURL: `${url}/api`,
    });
};
