/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { getPing } from '../Store/Actions/Ping';

const Ping = async () => {
    const dispatch = useDispatch();
    const value = await AsyncStorage.getItem('ping');
    return await dispatch(getPing(value))
        .unwrap()
        .then((res) => {
            console.log('ping : ', res);
            //return res.data;
        })
        .catch((err) => {
            console.log('error : ', err);
        });

    //return get;
};

export default Ping;
