/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useRef } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';
import * as Animatable from 'react-native-animatable';
import Home from '../Screens/Home';
import Control from '../Screens/Control';
import Report from '../Screens/Report';
import Notifikasi from '../Screens/Notifikasi';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useDispatch, useSelector } from 'react-redux';
import { loginSelector } from '../Store/Actions/Login';
import { getNotif } from '../Store/Actions/Notifikasi';
import Detail from '../Screens/Detail';
import Informasi from '../Screens/Informasi';
import AuthStack from './AuthStack';
import Profil from '../Screens/Profil';
import ViewPdf from '../Screens/ViewPdf';

const HomeStack = createNativeStackNavigator();
const ControlStack = createNativeStackNavigator();
const ReportStack = createNativeStackNavigator();

const HomeStackScreen = ({ navigation }) => (
    <HomeStack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
        <HomeStack.Screen
            name="Home"
            component={Home}
        />
    </HomeStack.Navigator>
);

const ControlStackScreen = ({ navigation }) => (
    <ControlStack.Navigator initialRouteName="CM" screenOptions={{ headerShown: false }}>
        <ControlStack.Screen
            name="CM"
            component={Control}
        />
    </ControlStack.Navigator>
);

const ReportSatckScreen = ({ navigation }) => (
    <ReportStack.Navigator initialRouteName="Report2" screenOptions={{ headerShown: false }}>
        <ReportStack.Screen
            name="Report2"
            component={Report}
        />
    </ReportStack.Navigator>
);

const tabArr = [
    {
        route: 'Beranda', label: 'Beranda', icon: 'Home', component: HomeStackScreen,
    },
    {
        route: 'Control', label: 'Control & Monitor', icon: 'Control', component: ControlStackScreen,
    },
    {
        route: 'Report', label: 'Report', icon: 'Report', component: ReportSatckScreen,
    },
];

const TabButton = (props) => {
    const { item, onPress, accessibilityState } = props;
    const focused = accessibilityState.selected;
    const viewRef = useRef(null);

    // useEffect(() => {
    //     if (focused) {
    //         viewRef.current.animate({ 0: { scale: 0.5, rotate: '0deg' }, 1: { scale: 1.5, rotate: '360deg' } });
    //     } else {
    //         viewRef.current.animate({ 0: { scale: 1.5, rotate: '360deg' }, 1: { scale: 1, rotate: '0deg' } });
    //     }
    // }, [focused]);

    const Ic = ({ val, focus }) => {
        if (val === 'Home') {
            return (
                focus ?
                    <Icons2.HomeIcon size={25} color={'#007aff'} />
                    :
                    <Icons.HomeIcon size={25} color={'#808080'} />
            );
        } else if (val === 'Control') {
            return (
                focus ?
                    <Icons2.LightBulbIcon size={25} color={'#007aff'} />
                    :
                    <Icons.LightBulbIcon size={25} color={'#808080'} />
            );
        } else {
            return (
                focus ?
                    <Icons2.DocumentReportIcon size={25} color={'#007aff'} />
                    :
                    <Icons.DocumentReportIcon size={25} color={'#808080'} />
            );
        }
    };

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={1}
            style={styles.container}>
            <Animatable.View
                ref={viewRef}
                duration={1000}
                style={styles.container}>
                <Ic val={item.icon} focus={focused} />
            </Animatable.View>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ color: focused ? '#007aff' : '#808080', fontSize: 13 }}>
                    {item.label}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

const Tab = createBottomTabNavigator();
const TabScreens = ({ navigation }) => {

    const dispatch = useDispatch();

    useEffect(() => {
        const getNotifikasi = async () => {
            const value = await AsyncStorage.getItem('token');
            let val = JSON.parse(value);
            if (value !== null) {
                //console.log(value);
                await dispatch(getNotif(val.accessToken)).unwrap();
            } else {
                console.log('Tidak Ada Token');
                clearInterval(dataInterval);
            }

            //await dispatch(getNotif(val.accessToken)).unwrap();
        };
        getNotifikasi();

        const dataInterval = setInterval(() => getNotifikasi(), 5 * 2000);
        return () => clearInterval(dataInterval);
    }, []);
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarStyle: {
                    height: 60,
                    backgroundColor: '#ffffff',
                    elevation: 3,
                },
            }}
        >
            {tabArr.map((item, index) => {
                return (
                    <Tab.Screen key={index} name={item.route} component={item.component}
                        options={{
                            tabBarShowLabel: false,
                            tabBarButton: (props) => <TabButton {...props} item={item} />,
                        }}
                    />
                );
            })}
        </Tab.Navigator>
    );
};

const Stack = createNativeStackNavigator();
const TabNavigator = () => {

    return (
        <Stack.Navigator
            initialRouteName="Tab"
        >
            <Stack.Screen
                name="Tab"
                component={TabScreens}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
            <Stack.Screen
                name="Notifikasi"
                component={Notifikasi}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
            <Stack.Screen
                name="Detail"
                component={Detail}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
            <Stack.Screen
                name="Informasi"
                component={Informasi}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
            <Stack.Screen
                name="Profil"
                component={Profil}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
            <Stack.Screen
                name="Pdf"
                component={ViewPdf}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
            <Stack.Screen
                name="Auth"
                component={AuthStack}
                options={{
                    headerShown: false,
                    animationEnable: true,
                    animation: 'fade',
                }}
            />
        </Stack.Navigator>
    );
};

export default TabNavigator;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
