/* eslint-disable prettier/prettier */
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import SplashScreen from '../Screens/SplashScreen';
import LoginScreen from '../Screens/LoginScreen';
import SetupScreen from '../Screens/SetupScreen';
import RegisterScreen from '../Screens/RegisterScreen';
import TabNavigator from './TabNavigator';
import LupaPassword from '../Screens/LupaPassword';

const Stack = createNativeStackNavigator();

const AuthStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Splash">
            <Stack.Screen name="Splash" component={SplashScreen} />
            <Stack.Screen name="Setup" component={SetupScreen} />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="Lupa" component={LupaPassword} />
            <Stack.Screen name="Tab2" component={TabNavigator} />
        </Stack.Navigator>
    );
};

export default AuthStack;
