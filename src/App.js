/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StatusBar,
  useColorScheme,
  ActivityIndicator,
  View,
  LogBox,
  Alert,
  Platform,
  PermissionsAndroid,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { Provider, useDispatch } from 'react-redux';
// import { PersistGate } from 'redux-persist/lib/integration/react';
import { store, persistor } from './Store';
import { PersistGate } from 'redux-persist/lib/integration/react';

import AuthStack from './Navigations/AuthStack';
import Lottie from 'lottie-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import TabNavigator from './Navigations/TabNavigator';
import PushNotification from 'react-native-push-notification';
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import { getLogin } from './Store/Actions/Login';
import { navigationRef } from './Navigations/Utils';
import { MenuProvider } from 'react-native-popup-menu';
import Toast, { BaseToast, InfoToast, ErrorToast } from 'react-native-toast-message';


function App() {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? '#000000' : '#ffffff',
  };
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        AsyncStorage.setItem('fcmToken', token.token);
        console.log('TOKEN:', token);
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  }, []);

  const toasConfig = {
    info: (props) => (
      <InfoToast
        {...props}
        text1Style={{
          fontSize: 15,
        }}
        text2Style={{
          fontSize: 13,
        }}
      />
    ),

    error: (props) => (
      <ErrorToast
        {...props}
        text1Style={{
          fontSize: 15,
        }}
        text2Style={{
          fontSize: 13,
        }}
      />
    ),

    warning: (props) => (
      <BaseToast
        {...props}
        style={{ borderLeftColor: 'orange' }}
        contentContainerStyle={{ paddingHorizontal: 15 }}
        text1Style={{
          fontSize: 15,
        }}
        text2Style={{
          fontSize: 13,
        }}
      />
    ),
  };

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('remoteMessage: ', remoteMessage);
      if (remoteMessage.notification?.title === 'Danger') {
        Toast.show({
          type: 'error',
          text1: remoteMessage.notification?.title + ' !',
          text2: remoteMessage.notification?.body,
          position: 'top',
          visibilityTime: 5000,
        });
      } else if (remoteMessage.notification?.title === 'Info') {
        Toast.show({
          type: 'info',
          text1: remoteMessage.notification?.title + ' !',
          text2: remoteMessage.notification?.body,
          position: 'top',
          visibilityTime: 5000,
        });
      } else {
        Toast.show({
          type: 'warning',
          text1: remoteMessage.notification?.title + ' !',
          text2: remoteMessage.notification?.body,
          position: 'top',
          visibilityTime: 5000,
        });
      }
      // Alert.alert(
      //   remoteMessage.notification?.title + ' !',
      //   remoteMessage.notification?.body,
      //   [
      //     {
      //       text: 'Cancel',
      //       onPress: () => console.log('Cancel Pressed'),
      //       style: 'cancel',
      //     },
      //     {
      //       text: 'OK', onPress: () => {
      //         console.log('OK Pressed');
      //         navigationRef.navigate('Notifikasi');
      //       },
      //     },
      //   ]
      // );
    });
    // Unmount FCM if done
    return unsubscribe;
  }, []);

  const init = async () => {
    await new Promise(resolve =>
      setTimeout(() => {
        resolve(true);
        setIsLoading(false);
      }, 3000),
    );
  };

  useEffect(() => {
    init();
  });

  useEffect(() => {
    const reqPermi = async () => {
      if (Platform.OS === 'android') {
        await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        );
      }
    };

    reqPermi();
  }, []);

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
        <Lottie source={require('./Assets/images/homescreen/3520-light-bulb.json')} autoPlay loop />
      </View>
    );
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <MenuProvider>
          <SafeAreaView style={backgroundStyle}>
            <StatusBar barStyle={'light-content'} backgroundColor={'transparent'} translucent />
            <NavigationContainer ref={navigationRef}>
              <Root />
              <Toast config={toasConfig} />
            </NavigationContainer>
          </SafeAreaView>
        </MenuProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;

const Root = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    flex: 1,
    backgroundColor: isDarkMode ? '#000000' : '#e7edf7',
  };

  const [token, setToken] = useState('');

  useEffect(() => {
    const getToken = async () => {
      const data = await AsyncStorage.getItem('token');
      //console.log(data);
      if (data) {
        console.log(data);
        setToken(data);
      } else {
        console.log('Tidak Ada Token');
        setToken('');
      }
    };

    getToken();
  }, []);


  // const value = AsyncStorage.getItem('token');
  // console.log(JSON.stringify(value));

  if (token !== '') {
    return (
      <TabNavigator />
    );
  } else {
    return (
      <AuthStack />
    );
  }

  // if (isSetup) {
  //   if (isLogin) {
  //     console.log(JSON.stringify(isSetup));
  //     return (
  //       <SafeAreaView style={backgroundStyle} >
  //         < StatusBar barStyle={'light-content'} backgroundColor={'transparent'} translucent />
  //         <NavigationContainer>
  //           <TabNavigator />
  //         </NavigationContainer>
  //       </SafeAreaView >
  //     );
  //   } else {
  //     return (
  //       <SafeAreaView style={backgroundStyle} >
  //         < StatusBar barStyle={'light-content'} backgroundColor={'transparent'} translucent />
  //         <NavigationContainer>
  //           <AuthStack />
  //         </NavigationContainer>
  //       </SafeAreaView >
  //     );
  //   }
  // } else {
  //   return (
  //     <SafeAreaView style={backgroundStyle}>
  //       <StatusBar barStyle={'light-content'} backgroundColor={'transparent'} translucent />
  //       <NavigationContainer>
  //         <AuthStack />
  //       </NavigationContainer>
  //     </SafeAreaView>
  //   );
  // }
};
