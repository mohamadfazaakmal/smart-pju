/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    FlatList,
    RefreshControl,
    ToastAndroid,
    ActivityIndicator,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import Header from '../Components/Header';
import { useDispatch, useSelector } from 'react-redux';
import { getMcps, mcpSelector } from '../Store/Actions/Mcp';
import AsyncStorage from '@react-native-async-storage/async-storage';

import CustomFlatList from '../Components/CustomFlatlist';

const Control = ({ navigation }) => {

    const dispatch = useDispatch();
    const data = useSelector(mcpSelector.selectAll);
    const [mcpData, setMcpData] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    const [loading, setLoading] = useState(true);

    // useEffect(() => {
    //     getData();
    // }, [dispatch]);

    const getData = async () => {
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);

        await dispatch(getMcps(val.accessToken)).unwrap();
        setRefreshing(false);
        setLoading(false);
        console.log(data);
    };

    useEffect(() => {
        const focusHandler = navigation.addListener('focus', () => {
            setLoading(true);
            getData();
            //ToastAndroid.show('Refresh..', ToastAndroid.SHORT);
        });
        return focusHandler;
    }, [navigation]);

    const onRefresh = () => {
        setRefreshing(true);
        getData();
    };

    if (loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                <ActivityIndicator size={'large'} color={'#0d73f0'} />
            </View>
        );
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#e7edf7' }}>
            <Header navigation={navigation} tinggi={130} judul={'Smart PJU'} />
            <Animatable.View animation="fadeInRightBig" style={{
                flexDirection: 'row',
                marginHorizontal: 50,
                marginTop: -20,
            }}>
                <View
                    style={{
                        flex: 1,
                        height: 50,
                        backgroundColor: '#ffffff',
                        borderRadius: 10,
                        padding: 5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 2,
                        borderBottomColor: 'transparent',
                        borderBottomWidth: 2,
                    }}
                >
                    <Text
                        style={{
                            fontSize: 20,
                            color: '#000033',
                            fontWeight: '500',
                        }}
                    >Data MCP</Text>
                </View>
            </Animatable.View>
            <Animatable.View
                style={{
                    flex: 1,
                }}
                animation="fadeInUpBig"
            >
                <FlatList
                    data={data}
                    renderItem={({ item, index }) => (
                        <CustomFlatList item={item} />
                    )}
                    ItemSeparatorComponent={false}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                            colors={['#0d73f0']}
                        />
                    }
                />
            </Animatable.View>
        </View>
    );
};

export default Control;
