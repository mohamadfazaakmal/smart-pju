/* eslint-disable prettier/prettier */
/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    Image,
    ActivityIndicator,
} from 'react-native';
import * as Icons from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';

import LoginSVG from '../Assets/images/misc/login.svg';
import GoogleSVG from '../Assets/images/misc/google.svg';
import FacebookSVG from '../Assets/images/misc/facebook.svg';
import TwitterSVG from '../Assets/images/misc/twitter.svg';

import CustomButton from '../Components/CustomButton';
import InputField from '../Components/InputField';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // install @hookform/resolvers (not @hookform/resolvers/yup)
import * as yup from 'yup';
import { postDaftar } from '../Store/Actions/Daftar';
import { Api } from '../Services/Api';

const schema = yup.object().shape({
    email: yup.string()
        .email('Email Tidak Valid')
        .required('Email Harus Diisi'),
});

const data = {
    email: '',
};

const LupaPassword = ({ navigation }) => {

    const { control, handleSubmit, formState: { errors }, setValue, reset, getValues, watch } = useForm({
        resolver: yupResolver(schema),
        mode: 'onBlur',
        reValidateMode: 'onBlur',
    });

    const [loading, setLoading] = useState(false);

    const masuk = async (value) => {
        console.log(value);
        setLoading(true);
        await Api.post('/forgot_password', {
            email: value.email,
        })
            .then((res) => {
                let row = res.data;
                console.log(row);
                if (row.status) {
                    setLoading(false);
                    navigation.navigate('Login');
                    Toast.show({
                        type: 'success',
                        text1: 'Berhasil!',
                        text2: 'Link Sudah Terkirim Ke Email, Silahkan Periksa Email Anda',
                        position: 'top',
                        visibilityTime: 5000,
                    });
                } else {
                    setLoading(false);
                    //console.log(res.data);
                    Toast.show({
                        type: 'error',
                        text1: 'Maaf!',
                        text2: row.message,
                        position: 'top',
                        visibilityTime: 5000,
                    });
                    //navigation.navigate('Login');
                }
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
                Toast.show({
                    type: 'error',
                    text1: 'Maaf!',
                    text2: 'Email tidak terdaftar, silahkan coba lagi',
                    position: 'top',
                    visibilityTime: 5000,
                });
            });
        //navigation.navigate('Login');
    };

    return (
        <Animatable.View style={{ flex: 1 }} animation="fadeIn" duration={1000}>
            <LinearGradient
                colors={['#2F80ED', '#5B86E5', '#36D1DC']}
                style={{ flex: 1 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ flex: 1, paddingHorizontal: 25, paddingTop: StatusBar.currentHeight }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ alignItems: 'center', paddingVertical: 50 }}>
                            <Image source={require('../Assets/images/forgot-password.png')} style={{ width: 150, height: 150 }} />
                            {/* <LoginSVG
                            height={300}
                            width={300}
                            style={{ transform: [{ rotate: '-5deg' }] }}
                        /> */}
                        </View>
                        <View style={{ paddingTop: 0 }}>
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Medium',
                                    fontSize: 25,
                                    fontWeight: '500',
                                    color: '#ffffff',
                                    marginBottom: 10,
                                    textAlign: 'center',
                                }}>
                                Lupa Password
                            </Text>
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Medium',
                                    fontSize: 17,
                                    fontWeight: '500',
                                    color: '#ffffff',
                                    marginBottom: 30,
                                    textAlign: 'center',
                                }}>
                                Silahkan Masukan Email Anda
                            </Text>
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.email ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.MailIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="email"
                                    defaultValue={data.email}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Email"
                                            keyboardType="email-address"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                        />
                                    )}
                                />
                            </View>
                            {errors?.email?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.email.message}</Text>
                            }
                        </View>

                        <TouchableOpacity
                            onPress={handleSubmit(masuk)}
                            style={{
                                backgroundColor: '#ffffff',
                                padding: 15,
                                borderRadius: 10,
                                marginBottom: 70,
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontWeight: '700',
                                    fontSize: 20,
                                    color: '#000000',
                                }}>
                                Kirim
                            </Text>
                        </TouchableOpacity>

                        {
                            loading ?
                                <View
                                    style={{ backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <ActivityIndicator size={'large'} color={'#ffffff'} />
                                </View>
                                : null
                        }

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                                marginVertical: 30,
                            }}>
                            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                <Text style={{
                                    color: '#ffffff',
                                    fontWeight: 'bold',
                                    fontStyle: 'italic',
                                    textDecorationLine: 'underline',
                                    fontSize: 16,
                                }}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <Toast />
            </LinearGradient>
        </Animatable.View>
    );
};

export default LupaPassword;
