/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import { View, Text, StatusBar, Image, TextInput, Dimensions, SafeAreaView, TouchableOpacity, ToastAndroid } from 'react-native';
import React, { useState, useEffect } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import Toast from 'react-native-toast-message';
import * as Icons from 'react-native-heroicons/solid';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { getPing, pingSelector } from '../Store/Actions/Ping';
import { navigationRef } from '../Navigations/Utils';

const SetupScreen = ({ navigation }) => {

    const dispatch = useDispatch();
    const dataUrl = useSelector(pingSelector);

    const Screen = Dimensions.get('window').width - 10;
    const [setup, setSetup] = useState('http://192.168.5.111:5000');
    const [check, setCheck] = useState(true);
    const [color, setColor] = useState(true);
    const [url, setUrl] = useState('');
    const pingg = async () => {
        const value = await AsyncStorage.getItem('ping');
        setUrl(value);
    };

    useEffect(() => {
        pingg();
    }, []);

    const textInput = (val) => {
        if (val.length !== 0) {
            setSetup(val);
            setColor(true);
            //setCheck(true);
        } else {
            setSetup(val);
            setColor(false);
            //setCheck(false);
        }
    };

    const storeData = async (value) => {
        try {
            await AsyncStorage.setItem('ping', value);
        } catch (e) {
            // saving error
        }
    };

    const simpan = async () => {
        if (setup.length !== 0) {
            dispatch(getPing(setup))
                .unwrap()
                .then(async (res) => {
                    //console.log(res);
                    navigation.navigate('Login');
                })
                .catch((e) => {
                    //console.log(e);
                });
            // await storeData(setup);
            // navigation.navigate('Login');
        } else {
            Toast.show({
                type: 'error',
                text1: 'Maaf',
                text2: 'URL tidak boleh kosong',
                position: 'top',
                visibilityTime: 5000,
            });
            setCheck(false);
            setColor(false);
        }
    };
    //console.log(url);
    return (
        <Animatable.View style={{ flex: 1 }} animation="fadeIn" duration={1000}>
            <LinearGradient
                colors={['#2F80ED', '#5B86E5', '#36D1DC']}
                style={{ flex: 1 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ paddingTop: StatusBar.currentHeight, flex: 1, paddingHorizontal: 25 }}>
                    <View style={{ marginTop: 50 }}>
                        <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#FFFFFF', fontSize: 35, fontWeight: '700' }}>
                                Smart PJU
                            </Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../Assets/images/idea.png')} style={{ width: 130, height: 130 }} />
                        </View>
                        <View style={{ marginTop: 70, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#FFFFFF', fontSize: 30, fontWeight: '700' }}>
                                Setup URL
                            </Text>
                        </View>
                        <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#FFFFFF', fontSize: 25, fontWeight: '200' }}>
                                Masukan URL Anda!
                            </Text>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: color ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginVertical: 25,
                            }}>
                            <Icons.LinkIcon size={20} color={color ? '#ffffff' : '#c72020'} />
                            <TextInput
                                value={setup}
                                onChangeText={text => textInput(text)}
                                placeholder="URL"
                                keyboardType="url"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                placeholderTextColor="#ffffff"
                            />
                        </View>
                        <View style={{ marginVertical: 10 }}>
                            <TouchableOpacity
                                onPress={() => { simpan(); }}
                                style={{
                                    backgroundColor: '#ffffff',
                                    padding: 15,
                                    borderRadius: 10,
                                    marginBottom: 100,
                                }}>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontWeight: '700',
                                        fontSize: 20,
                                        color: '#000000',
                                    }}>
                                    Simpan
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <Toast />
            </LinearGradient>
        </Animatable.View>
    );
};

export default SetupScreen;
