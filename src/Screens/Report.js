/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    FlatList,
    RefreshControl,
    Button,
    TextInput,
    SafeAreaView,
    ActivityIndicator,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';
import Header from '../Components/Header';
import { useDispatch, useSelector } from 'react-redux';
import { getMcps, mcpSelector } from '../Store/Actions/Mcp';
import AsyncStorage from '@react-native-async-storage/async-storage';

import CustomFlatList from '../Components/CustomFlatlist';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { Picker } from '@react-native-picker/picker';
import moment from 'moment';
import 'moment/locale/id';
import DatePicker from 'react-native-modern-datepicker';
import getYear from 'date-fns/getYear';
import range from 'lodash/range';
import { Api } from '../Services/Api';
import { pingSelector } from '../Store/Actions/Ping';
import axios from 'axios';
import Loader from '../Components/Loader';

const Report = ({ navigation }) => {

    const tgl = moment(new Date()).format('YYYY-MM-DD');
    const bln = moment(new Date()).format('YYYY-MM');
    const thn = moment(new Date()).format('YYYY');
    //console.log(thn);
    const years = range(2021, getYear(new Date()) + 1, 1);

    const [jamDari, setJamDari] = useState('00');
    const [jamSampai, setJamSampai] = useState('00');
    const [jamDari2, setJamDari2] = useState('00');
    const [jamSampai2, setJamSampai2] = useState('00');
    const [tanggalDari, setTanggalDari] = useState(tgl);
    const [tanggalDari2, setTanggalDari2] = useState(tgl);
    const [visibleTanggalDari, setVisibleTanggalDari] = useState(false);
    const [tanggalSampai, setTanggalSampai] = useState(tgl);
    const [tanggalSampai2, setTanggalSampai2] = useState(tgl);
    const [visibleTanggalSampai, setVisibleTanggalSampai] = useState(false);
    const [bulanDari, setBulanDari] = useState(bln);
    const [bulanDari2, setBulanDari2] = useState(bln);
    const [bulanSampai, setBulanSampai] = useState(bln);
    const [bulanSampai2, setBulanSampai2] = useState(bln);
    const [tahunDari, setTahunDari] = useState(thn);
    const [tahunDari2, setTahunDari2] = useState(thn);
    const [tahunSampai, setTahunSampai] = useState(thn);
    const [tahunSampai2, setTahunSampai2] = useState(thn);
    const [mcp, setMcp] = useState('0');
    const [mcp2, setMcp2] = useState('0');
    const [interval, setInterval] = useState('');
    const dataMcp = useSelector(mcpSelector.selectAll);
    const [showJam, setShowJam] = useState(false);
    const [showHari, setShowHari] = useState(false);
    const [showBulan, setShowBulan] = useState(false);
    const [showTahun, setShowTahun] = useState(false);
    const [dataJam, setDataJam] = useState({});
    const [dataHari, setDataHari] = useState({});
    const [dataBulan, setDataBulan] = useState({});
    const [dataTahun, setDataTahun] = useState({});
    const [showDownload, setShowDownload] = useState(false);
    const [url, setUrl] = useState('');
    const [filename, setFilename] = useState('');
    const [loading, setLoading] = useState(true);
    const [warna, setWarna] = useState(0);
    const [munyer, setMunyer] = useState(false);
    //const baseUrl = useSelector(pingSelector);

    const showTanggalDari = () => {
        setVisibleTanggalDari(true);
    };

    const hideTanggalDari = () => {
        setVisibleTanggalDari(false);
    };

    const handleTanggalDari = (time) => {
        let timer = moment(time).format('YYYY-MM-DD');
        setTanggalDari(String(timer));
        console.log('A date from has been picked: ', timer);
        hideTanggalDari();
    };

    const showTanggalSampai = () => {
        setVisibleTanggalSampai(true);
    };

    const hideTanggalSampai = () => {
        setVisibleTanggalSampai(false);
    };

    const handleTanggalSampai = (time) => {
        let timer = moment(time).format('YYYY-MM-DD');
        setTanggalSampai(String(timer));
        console.log('A date to has been picked: ', timer);
        hideTanggalSampai();
    };

    function sortByTotal(a, b) {
        //return b.total_kwh - a.total_kwh
        const n = b.total_kwh - a.total_kwh;
        // sort by listId
        //if (n !== 0) {
        return n;
        //}
        // if listId is equal then sort by name
        //return a.name.localeCompare(b.name);
    }

    useEffect(() => {
        const focusHandler = navigation.addListener('focus', () => {
            setLoading(true);
            setDataJam({});
            setDataHari({});
            setDataBulan({});
            setDataTahun({});
            setMcp('0');
            setInterval('');
            setJamDari('00');
            setJamSampai('00');
            setTanggalDari(tgl);
            setTanggalSampai(tgl);
            setBulanDari(bln);
            setBulanSampai(bln);
            setTahunDari(thn);
            setTahunSampai(thn);

            setShowJam(false);
            setShowHari(false);
            setShowBulan(false);
            setShowTahun(false);
            setTimeout(() => {
                setLoading(false);
            }, 500);
        });
        return focusHandler;
    }, [navigation]);

    const goDownload = () => {
        navigation.navigate('Pdf', {
            url: url,
            filename: filename,
        });
    };

    const GetPerjam = async () => {
        setMunyer(true);
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);
        //console.log(val.accessToken);
        const baseUrl = await AsyncStorage.getItem('ping');
        await axios.get(`${baseUrl}/api/report`, {
            headers: { Authorization: 'Bearer ' + val.accessToken },
            params: {
                interval: interval,
                date: tgl,
                dari_jam: jamDari,
                sampai_jam: jamSampai,
                mcp_id: mcp,
            },
        })
            .then(async (res) => {
                let row = res?.data?.data;
                console.log(row);

                let terbesar = 0;
                row.report.map((d) => {
                    if (d.total_kwh > terbesar) {
                        terbesar = d.total_kwh;
                    } else {
                        terbesar = terbesar;
                    }
                });
                console.log('Total Terbesar : ', terbesar);
                setWarna(terbesar);

                setMcp2(mcp);
                setJamDari2(jamDari);
                setJamSampai2(jamSampai);
                setDataJam(row);
                setShowJam(true);

                setShowHari(false);
                setShowBulan(false);
                setShowTahun(false);
                setShowDownload(true);

                setDataHari({});
                setDataBulan({});
                setDataTahun({});

                setMunyer(false);

                await axios.get(`${baseUrl}/api/report/export_pdf`, {
                    headers: { Authorization: 'Bearer ' + val.accessToken },
                    params: {
                        interval: interval,
                        date: tgl,
                        dari_jam: jamDari,
                        sampai_jam: jamSampai,
                        mcp_id: mcp,
                    },
                })
                    .then((qry) => {
                        console.log(qry.data);
                        let url2 = qry?.data?.url;
                        setUrl(url2);
                        setFilename('report_perjam_' + mcp2 + '_' + jamDari + '.00_' + jamSampai + '.00');
                        setShowDownload(false);
                    })
                    .catch((e) => {
                        console.log(e);
                    });
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const GetPerHari = async () => {
        setMunyer(true);
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);
        const baseUrl = await AsyncStorage.getItem('ping');
        //console.log(val.accessToken);
        await axios.get(`${baseUrl}/api/report`, {
            headers: { Authorization: 'Bearer ' + val.accessToken },
            params: {
                interval: interval,
                dari_tanggal: tanggalDari,
                sampai_tanggal: tanggalSampai,
                mcp_id: mcp,
            },
        })
            .then(async (res) => {
                let row = res?.data?.data;
                console.log(row);

                let terbesar = 0;
                row.report.map((d) => {
                    if (d.total_kwh > terbesar) {
                        terbesar = d.total_kwh;
                    } else {
                        terbesar = terbesar;
                    }
                });
                console.log('Total Terbesar : ', terbesar);
                setWarna(terbesar);

                setMcp2(mcp);
                setTanggalDari2(tanggalDari);
                setTanggalSampai2(tanggalSampai);
                setDataHari(row);
                setShowJam(false);
                setShowHari(true);
                setShowBulan(false);
                setShowTahun(false);
                setShowDownload(true);

                setDataJam({});
                setDataBulan({});
                setDataTahun({});

                setMunyer(false);

                await axios.get(`${baseUrl}/api/report/export_pdf`, {
                    headers: { Authorization: 'Bearer ' + val.accessToken },
                    params: {
                        interval: interval,
                        dari_tanggal: tanggalDari,
                        sampai_tanggal: tanggalSampai,
                        mcp_id: mcp,
                    },
                })
                    .then((qry) => {
                        console.log(qry.data);
                        let url2 = qry?.data?.url;
                        setUrl(url2);
                        setFilename('report_pertanggal_' + mcp2 + '_' + tanggalDari + '_' + tanggalSampai);
                        setShowDownload(false);
                    })
                    .catch((e) => {
                        console.log(e);
                    });
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const GetPerBulan = async () => {
        setMunyer(true);
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);
        const baseUrl = await AsyncStorage.getItem('ping');
        console.log(baseUrl);
        await axios.get(`${baseUrl}/api/report`, {
            headers: { Authorization: 'Bearer ' + val.accessToken },
            params: {
                interval: interval,
                dari_bulan: bulanDari,
                sampai_bulan: bulanSampai,
                mcp_id: mcp,
            },
        })
            .then(async (res) => {
                let row = res?.data?.data;
                console.log(row);

                let terbesar = 0;
                row.report.map((d) => {
                    if (d.total_kwh > terbesar) {
                        terbesar = d.total_kwh;
                    } else {
                        terbesar = terbesar;
                    }
                });
                console.log('Total Terbesar : ', terbesar);
                setWarna(terbesar);

                setMcp2(mcp);
                setBulanDari2(bulanDari);
                setBulanSampai2(bulanSampai);
                setDataBulan(row);

                setShowJam(false);
                setShowHari(false);
                setShowBulan(true);
                setShowTahun(false);
                setShowDownload(true);

                setDataJam({});
                setDataHari({});
                setDataTahun({});

                setMunyer(false);

                await axios.get(`${baseUrl}/api/report/export_pdf`, {
                    headers: { Authorization: 'Bearer ' + val.accessToken },
                    params: {
                        interval: interval,
                        dari_bulan: bulanDari,
                        sampai_bulan: bulanSampai,
                        mcp_id: mcp,
                    },
                })
                    .then((qry) => {
                        console.log(qry.data);
                        let url2 = qry?.data?.url;
                        setUrl(url2);
                        setFilename('report_perbulan_' + mcp2 + '_' + bulanDari + '_' + bulanSampai);
                        setShowDownload(false);
                    })
                    .catch((e) => {
                        console.log(e);
                    });
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const GetPerTahun = async () => {
        setMunyer(true);
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);
        const baseUrl = await AsyncStorage.getItem('ping');
        //console.log(val.accessToken);
        await axios.get(`${baseUrl}/api/report`, {
            headers: { Authorization: 'Bearer ' + val.accessToken },
            params: {
                interval: interval,
                dari_tahun: tahunDari,
                sampai_tahun: tahunSampai,
                mcp_id: mcp,
            },
        })
            .then(async (res) => {
                let row = res?.data?.data;
                console.log(row);

                let terbesar = 0;
                row.report.map((d) => {
                    if (d.total_kwh > terbesar) {
                        terbesar = d.total_kwh;
                    } else {
                        terbesar = terbesar;
                    }
                });
                console.log('Total Terbesar : ', terbesar);
                setWarna(terbesar);

                setMcp2(mcp);
                setTahunDari2(tahunDari);
                setTahunSampai2(tahunSampai);
                setDataTahun(row);

                setShowJam(false);
                setShowHari(false);
                setShowBulan(false);
                setShowTahun(true);
                setShowDownload(true);

                setDataJam({});
                setDataHari({});
                setDataBulan({});

                setMunyer(false);

                await axios.get(`${baseUrl}/api/report/export_pdf`, {
                    headers: { Authorization: 'Bearer ' + val.accessToken },
                    params: {
                        interval: interval,
                        dari_tahun: tahunDari,
                        sampai_tahun: tahunSampai,
                        mcp_id: mcp,
                    },
                })
                    .then((qry) => {
                        console.log(qry.data);
                        let url2 = qry?.data?.url;
                        setUrl(url2);
                        setFilename('report_pertahun_' + mcp2 + '_' + tahunDari + '_' + tahunSampai);
                        setShowDownload(false);
                    })
                    .catch((e) => {
                        console.log(e);
                    });
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const filter = () => {
        if (interval === '0') {
            if (jamDari >= jamSampai) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf !',
                    text2: 'Pilihan Jam Tidak Valid',
                    position: 'top',
                    visibilityTime: 5000,
                });
            } else {
                GetPerjam();
            }
        } else if (interval === '1') {
            GetPerHari();
        } else if (interval === '2') {
            if (bulanDari > bulanSampai) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf !',
                    text2: 'Pilihan Bulan Tidak Valid',
                    position: 'top',
                    visibilityTime: 5000,
                });
            } else {
                GetPerBulan();
            }
        } else if (interval === '3') {
            if (tahunDari > tahunSampai) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf !',
                    text2: 'Pilihan Tahun Tidak Valid',
                    position: 'top',
                    visibilityTime: 5000,
                });
            } else {
                GetPerTahun();
            }
        } else {
            Toast.show({
                type: 'error',
                text1: 'Maaf !',
                text2: 'Harap Memilih Interval Terlebih Dahulu',
                position: 'top',
                visibilityTime: 5000,
            });
        }
    };

    const Option = () => {
        if (interval === '0') {
            return (
                <>
                    <View style={{
                        marginTop: 10,
                    }}>
                        <Text
                            style={{
                                color: '#708090',
                                fontSize: 15,
                            }}
                        >Pilih Jam</Text>
                    </View><View
                        style={{ flexDirection: 'row' }}
                    >
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{
                                    width: '100%',
                                    color: '#ffffff',
                                    backgroundColor: '#1ad1ff',
                                    marginVertical: 5,
                                    borderRadius: 5,
                                    height: 50,
                                }}
                            >
                                <Picker
                                    mode="dropdown"
                                    selectedValue={jamDari}
                                    onValueChange={(itemValue, itemIndex) => setJamDari(itemValue)}
                                >
                                    <Picker.Item label="00:00" value="00" />
                                    <Picker.Item label="01:00" value="01" />
                                    <Picker.Item label="02:00" value="02" />
                                    <Picker.Item label="03:00" value="03" />
                                    <Picker.Item label="04:00" value="04" />
                                    <Picker.Item label="05:00" value="05" />
                                    <Picker.Item label="06:00" value="06" />
                                    <Picker.Item label="07:00" value="07" />
                                    <Picker.Item label="08:00" value="08" />
                                    <Picker.Item label="09:00" value="09" />
                                    <Picker.Item label="10:00" value="10" />
                                    <Picker.Item label="11:00" value="11" />
                                    <Picker.Item label="12:00" value="12" />
                                    <Picker.Item label="13:00" value="13" />
                                    <Picker.Item label="14:00" value="14" />
                                    <Picker.Item label="15:00" value="15" />
                                    <Picker.Item label="16:00" value="16" />
                                    <Picker.Item label="17:00" value="17" />
                                    <Picker.Item label="18:00" value="18" />
                                    <Picker.Item label="19:00" value="19" />
                                    <Picker.Item label="20:00" value="20" />
                                    <Picker.Item label="21:00" value="21" />
                                    <Picker.Item label="22:00" value="22" />
                                    <Picker.Item label="23:00" value="23" />
                                </Picker>
                            </View>
                        </View>
                        <View
                            style={{ marginHorizontal: 5, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text
                                style={{
                                    color: '#708090',
                                    fontSize: 15,
                                }}
                            >s/d</Text>
                        </View>
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{
                                    width: '100%',
                                    color: '#ffffff',
                                    backgroundColor: '#1ad1ff',
                                    marginVertical: 5,
                                    borderRadius: 5,
                                    height: 50,
                                }}
                            >
                                <Picker
                                    mode="dropdown"
                                    selectedValue={jamSampai}
                                    onValueChange={(itemValue, itemIndex) => setJamSampai(itemValue)}
                                >
                                    <Picker.Item label="00:00" value="00" />
                                    <Picker.Item label="01:00" value="01" />
                                    <Picker.Item label="02:00" value="02" />
                                    <Picker.Item label="03:00" value="03" />
                                    <Picker.Item label="04:00" value="04" />
                                    <Picker.Item label="05:00" value="05" />
                                    <Picker.Item label="06:00" value="06" />
                                    <Picker.Item label="07:00" value="07" />
                                    <Picker.Item label="08:00" value="08" />
                                    <Picker.Item label="09:00" value="09" />
                                    <Picker.Item label="10:00" value="10" />
                                    <Picker.Item label="11:00" value="11" />
                                    <Picker.Item label="12:00" value="12" />
                                    <Picker.Item label="13:00" value="13" />
                                    <Picker.Item label="14:00" value="14" />
                                    <Picker.Item label="15:00" value="15" />
                                    <Picker.Item label="16:00" value="16" />
                                    <Picker.Item label="17:00" value="17" />
                                    <Picker.Item label="18:00" value="18" />
                                    <Picker.Item label="19:00" value="19" />
                                    <Picker.Item label="20:00" value="20" />
                                    <Picker.Item label="21:00" value="21" />
                                    <Picker.Item label="22:00" value="22" />
                                    <Picker.Item label="23:00" value="23" />
                                </Picker>
                            </View>
                        </View>
                    </View>
                </>
            );
        } else if (interval === '1') {
            return (
                <View>
                    <View
                        style={{ marginTop: 10 }}
                    >
                        <Text
                            style={{
                                color: '#708090',
                                fontSize: 15,
                            }}
                        >Pilih Tanggal</Text>
                    </View>
                    <View
                        style={{ flexDirection: 'row', marginTop: 5 }}
                    >
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{ flexDirection: 'row' }}
                            >
                                <TouchableOpacity
                                    onPress={showTanggalDari}
                                    style={{
                                        borderBottomLeftRadius: 5,
                                        borderTopLeftRadius: 5,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        backgroundColor: '#1ad1ff',
                                        paddingHorizontal: 10,
                                        height: 50,
                                    }}
                                >
                                    <Icons.CalendarIcon size={25} color={'#ffffff'} />
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    date={new Date(tanggalDari)}
                                    isVisible={visibleTanggalDari}
                                    textColor="blue"
                                    mode="date"
                                    locale="id"
                                    maximumDate={new Date(tanggalSampai)}
                                    onConfirm={handleTanggalDari}
                                    onCancel={hideTanggalDari} />
                                <TextInput
                                    style={{
                                        flex: 1,
                                        paddingVertical: 0,
                                        color: '#666666',
                                        backgroundColor: '#f2f2f2',
                                        borderTopRightRadius: 5,
                                        borderBottomRightRadius: 5,
                                    }}
                                    //onChangeText={jam}
                                    value={tanggalDari}
                                    placeholderTextColor="#666666"
                                    selectionColor={'#666666'}
                                    editable={false} />
                            </View>
                        </View>
                        <View
                            style={{ marginHorizontal: 5, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text
                                style={{
                                    color: '#708090',
                                    fontSize: 15,
                                }}
                            >s/d</Text>
                        </View>
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{ flexDirection: 'row' }}
                            >
                                <TouchableOpacity
                                    onPress={showTanggalSampai}
                                    style={{
                                        borderBottomLeftRadius: 5,
                                        borderTopLeftRadius: 5,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        backgroundColor: '#1ad1ff',
                                        paddingHorizontal: 10,
                                        height: 50,
                                    }}
                                >
                                    <Icons.CalendarIcon size={25} color={'#ffffff'} />
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    date={new Date(tanggalSampai)}
                                    isVisible={visibleTanggalSampai}
                                    textColor="blue"
                                    mode="date"
                                    locale="id"
                                    minimumDate={new Date(tanggalDari)}
                                    maximumDate={new Date()}
                                    onConfirm={handleTanggalSampai}
                                    onCancel={hideTanggalSampai}
                                />
                                <TextInput
                                    style={{
                                        flex: 1,
                                        paddingVertical: 0,
                                        color: '#666666',
                                        backgroundColor: '#f2f2f2',
                                        borderTopRightRadius: 5,
                                        borderBottomRightRadius: 5,
                                        marginRight: 5,
                                    }}
                                    //onChangeText={jam}
                                    value={tanggalSampai}
                                    placeholderTextColor="#666666"
                                    selectionColor={'#666666'}
                                    editable={false} />
                            </View>
                        </View>
                    </View>
                </View>
            );
        } else if (interval === '2') {
            let tahun = moment(new Date()).format('YYYY');
            return (
                <View>
                    <View style={{
                        marginTop: 10,
                    }}>
                        <Text
                            style={{
                                color: '#708090',
                                fontSize: 15,
                            }}
                        >Pilih Bulan</Text>
                    </View><View
                        style={{ flexDirection: 'row' }}
                    >
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{
                                    width: '100%',
                                    color: '#ffffff',
                                    backgroundColor: '#1ad1ff',
                                    marginVertical: 5,
                                    borderRadius: 5,
                                    height: 50,
                                }}
                            >
                                <Picker
                                    mode="dropdown"
                                    selectedValue={bulanDari}
                                    onValueChange={(itemValue, itemIndex) => {
                                        setBulanDari(itemValue);
                                        console.log('Dari Bulan : ', itemValue);
                                    }}>
                                    <Picker.Item label={moment(tahun + '-01').format('MMMM')} value={tahun + '-01'} />
                                    <Picker.Item label={moment(tahun + '-02').format('MMMM')} value={tahun + '-02'} />
                                    <Picker.Item label={moment(tahun + '-03').format('MMMM')} value={tahun + '-03'} />
                                    <Picker.Item label={moment(tahun + '-04').format('MMMM')} value={tahun + '-04'} />
                                    <Picker.Item label={moment(tahun + '-05').format('MMMM')} value={tahun + '-05'} />
                                    <Picker.Item label={moment(tahun + '-06').format('MMMM')} value={tahun + '-06'} />
                                    <Picker.Item label={moment(tahun + '-07').format('MMMM')} value={tahun + '-07'} />
                                    <Picker.Item label={moment(tahun + '-08').format('MMMM')} value={tahun + '-08'} />
                                    <Picker.Item label={moment(tahun + '-09').format('MMMM')} value={tahun + '-09'} />
                                    <Picker.Item label={moment(tahun + '-10').format('MMMM')} value={tahun + '-10'} />
                                    <Picker.Item label={moment(tahun + '-11').format('MMMM')} value={tahun + '-11'} />
                                    <Picker.Item label={moment(tahun + '-12').format('MMMM')} value={tahun + '-12'} />
                                </Picker>
                            </View>
                        </View>
                        <View
                            style={{ marginHorizontal: 5, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text
                                style={{
                                    color: '#708090',
                                    fontSize: 15,
                                }}
                            >s/d</Text>
                        </View>
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{
                                    width: '100%',
                                    color: '#ffffff',
                                    backgroundColor: '#1ad1ff',
                                    marginVertical: 5,
                                    borderRadius: 5,
                                    height: 50,
                                }}
                            >
                                <Picker
                                    mode="dropdown"
                                    selectedValue={bulanSampai}
                                    onValueChange={(itemValue, itemIndex) => {
                                        setBulanSampai(itemValue);
                                        console.log('Sampai Bulan : ', itemValue);
                                    }}>
                                    <Picker.Item label={moment(tahun + '-01').format('MMMM')} value={tahun + '-01'} />
                                    <Picker.Item label={moment(tahun + '-02').format('MMMM')} value={tahun + '-02'} />
                                    <Picker.Item label={moment(tahun + '-03').format('MMMM')} value={tahun + '-03'} />
                                    <Picker.Item label={moment(tahun + '-04').format('MMMM')} value={tahun + '-04'} />
                                    <Picker.Item label={moment(tahun + '-05').format('MMMM')} value={tahun + '-05'} />
                                    <Picker.Item label={moment(tahun + '-06').format('MMMM')} value={tahun + '-06'} />
                                    <Picker.Item label={moment(tahun + '-07').format('MMMM')} value={tahun + '-07'} />
                                    <Picker.Item label={moment(tahun + '-08').format('MMMM')} value={tahun + '-08'} />
                                    <Picker.Item label={moment(tahun + '-09').format('MMMM')} value={tahun + '-09'} />
                                    <Picker.Item label={moment(tahun + '-10').format('MMMM')} value={tahun + '-10'} />
                                    <Picker.Item label={moment(tahun + '-11').format('MMMM')} value={tahun + '-11'} />
                                    <Picker.Item label={moment(tahun + '-12').format('MMMM')} value={tahun + '-12'} />
                                </Picker>
                            </View>
                        </View>
                    </View>
                </View>
            );
        } else if (interval === '3') {
            return (
                <View>
                    <View style={{
                        marginTop: 10,
                    }}>
                        <Text
                            style={{
                                color: '#708090',
                                fontSize: 15,
                            }}
                        >Pilih Tahun</Text>
                    </View><View
                        style={{ flexDirection: 'row' }}
                    >
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{
                                    width: '100%',
                                    color: '#ffffff',
                                    backgroundColor: '#1ad1ff',
                                    marginVertical: 5,
                                    borderRadius: 5,
                                    height: 50,
                                }}
                            >
                                <Picker
                                    mode="dropdown"
                                    selectedValue={tahunDari}
                                    onValueChange={(itemValue, itemIndex) => {
                                        console.log('Dari Tahun : ', itemValue);
                                        setTahunDari(itemValue);
                                    }}>
                                    {
                                        years.map((val) => {
                                            return (
                                                <Picker.Item key={val} label={String(val)} value={String(val)} />
                                            );
                                        }
                                        )
                                    }
                                </Picker>
                            </View>
                        </View>
                        <View
                            style={{ marginHorizontal: 5, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text
                                style={{
                                    color: '#708090',
                                    fontSize: 15,
                                }}
                            >s/d</Text>
                        </View>
                        <View
                            style={{ flex: 1 }}
                        >
                            <View
                                style={{
                                    width: '100%',
                                    color: '#ffffff',
                                    backgroundColor: '#1ad1ff',
                                    marginVertical: 5,
                                    borderRadius: 5,
                                    height: 50,
                                }}
                            >
                                <Picker
                                    mode="dropdown"
                                    selectedValue={tahunSampai}
                                    onValueChange={(itemValue, itemIndex) => {
                                        console.log('Sampai Tahun : ', itemValue);
                                        setTahunSampai(itemValue);
                                    }}>
                                    {
                                        years.map((val) => {
                                            return (
                                                <Picker.Item key={val} label={String(val)} value={String(val)} />
                                            );
                                        }
                                        )
                                    }
                                </Picker>
                            </View>
                        </View>
                    </View>
                </View>
            );
        } else {
            return null;
        }
    };

    //console.log(jam);

    if (loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                <ActivityIndicator size={'large'} color={'#0d73f0'} />
            </View>
        );
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#e7edf7' }}>
            <Loader loading={munyer} />
            <Header navigation={navigation} tinggi={130} judul={'Smart PJU'} />
            <Animatable.View animation="fadeInRightBig" style={{
                flexDirection: 'row',
                marginHorizontal: 50,
                marginTop: -20,
            }}>
                <View
                    style={{
                        flex: 1,
                        height: 50,
                        backgroundColor: '#ffffff',
                        borderRadius: 10,
                        padding: 5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 2,
                        borderBottomColor: 'transparent',
                        borderBottomWidth: 2,
                    }}
                >
                    <Text
                        style={{
                            fontSize: 20,
                            color: '#000033',
                            fontWeight: '500',
                        }}
                    >Report</Text>
                </View>
            </Animatable.View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Animatable.View
                    style={{
                        marginHorizontal: 10,
                    }}
                    animation="fadeInUpBig"
                >
                    <View
                        style={{
                            backgroundColor: '#ffffff',
                            borderWidth: 1,
                            borderColor: 'transparent',
                            marginTop: 10,
                            shadowOffset: 2,
                            shadowColor: 'grey',
                            shadowOpacity: 2,
                            elevation: 2,
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: '#0095ef',
                                padding: 10,
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}
                        >
                            <Icons.FilterIcon size={20} color={'#ffffff'} />
                            <Text style={{ fontSize: 16, fontWeight: '500', color: '#ffffff', marginLeft: 5 }}>Filter Data</Text>
                        </View>
                        <View
                            style={{
                                marginHorizontal: 10,
                                marginVertical: 10,
                            }}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <View
                                    style={{
                                        flex: 1,
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: '#708090',
                                            fontSize: 15,
                                        }}
                                    >MCP</Text>
                                    <View
                                        style={{
                                            width: '100%',
                                            color: '#ffffff',
                                            backgroundColor: '#1ad1ff',
                                            marginVertical: 5,
                                            borderRadius: 5,
                                            height: 50,
                                        }}
                                    >
                                        <Picker
                                            mode="dialog"
                                            selectedValue={mcp}
                                            onValueChange={(itemValue, itemIndex) => {
                                                setMcp(itemValue);
                                                console.log(itemValue);
                                            }}
                                        >
                                            <Picker.Item label="Semua MCP" value="0" key={0} />
                                            {dataMcp.map((val) => <Picker.Item label={val.name} value={val.id} key={val.id} />
                                            )}
                                        </Picker>
                                    </View>
                                </View>
                                <View style={{ width: 5 }}></View>
                                <View
                                    style={{
                                        flex: 1,
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: '#708090',
                                            fontSize: 15,
                                        }}
                                    >Interval</Text>
                                    <View
                                        style={{
                                            width: '100%',
                                            color: '#ffffff',
                                            backgroundColor: '#1ad1ff',
                                            marginVertical: 5,
                                            borderRadius: 5,
                                            height: 50,
                                        }}
                                    >
                                        <Picker
                                            mode="dialog"
                                            selectedValue={interval}
                                            onValueChange={(itemValue, itemIndex) => {
                                                setInterval(itemValue);
                                                console.log('Interval : ', itemValue);
                                            }}
                                        >
                                            <Picker.Item label="Pilih Interval" value="" />
                                            <Picker.Item label="Jam" value="0" />
                                            <Picker.Item label="Tanggal" value="1" />
                                            <Picker.Item label="Bulan" value="2" />
                                            <Picker.Item label="Tahun" value="3" />
                                        </Picker>
                                    </View>
                                </View>
                            </View>
                            <Option />
                        </View>
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                            }}
                        >
                            <TouchableOpacity
                                onPress={filter}
                                style={{
                                    backgroundColor: '#0095ef',
                                    paddingVertical: 10,
                                    paddingHorizontal: 25,
                                    borderRadius: 5,
                                    marginRight: 10,
                                    marginBottom: 10,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        color: '#ffffff',
                                        fontSize: 15,
                                    }}
                                >Filter</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Animatable.View>
                {
                    showJam &&
                    <Animatable.View
                        animation="fadeIn"
                        style={{
                            marginHorizontal: 10,
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: '#ffffff',
                                borderWidth: 1,
                                borderColor: 'transparent',
                                marginTop: 10,
                                padding: 10,
                                shadowOffset: 2,
                                shadowColor: 'grey',
                                shadowOpacity: 2,
                                elevation: 2,
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 5,
                                }}
                            >
                                <View>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Detail Report {
                                        mcp2 === '0' ?
                                            'Semua MCP'
                                            :
                                            dataJam.report.filter((val) => val.mcp_id === mcp2).slice(0, 1).map((res) =>
                                                res.name
                                            )
                                    }</Text>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Dari Jam {jamDari2}.00 s/d Jam {jamSampai2}.00</Text>
                                </View>
                                <View>
                                    {
                                        showDownload ?
                                            <ActivityIndicator />
                                            :
                                            <TouchableOpacity
                                                onPress={goDownload}
                                                style={{
                                                    backgroundColor: '#0095ef',
                                                    paddingVertical: 10,
                                                    paddingHorizontal: 15,
                                                    borderRadius: 5,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#ffffff',
                                                        fontSize: 15,
                                                    }}
                                                >Download</Text>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </View>
                            {
                                dataJam.report.map((val, index) =>
                                    <View key={index} style={{ marginTop: 5 }}>
                                        <View
                                            style={{
                                                flexDirection: 'row',

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Jam :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.jam}
                                            </Text>
                                        </View>
                                        {
                                            mcp2 === '0' ?
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                    }}
                                                >
                                                    <Text
                                                        style={{
                                                            color: '#666666',
                                                        }}
                                                    >
                                                        {val.name}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                        <View
                                            style={{
                                                marginLeft: 20,
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar R :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_r} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar S :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_s} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar T :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_t} Watt
                                                </Text>
                                            </View>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                backgroundColor: val.total_kwh === warna ? '#79ff4d' : '#ffffff',
                                                marginHorizontal: -10,
                                                paddingHorizontal: 10,

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Sub Total :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.total_kwh} Watt
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                height: 1,
                                                backgroundColor: '#666666',
                                                opacity: 0.4,
                                                marginVertical: 10,
                                            }}
                                        />
                                    </View>
                                )
                            }
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',

                                }}
                            >
                                <Text
                                    style={{
                                        color: '#333333',
                                    }}
                                >
                                    Total :
                                </Text>
                                <Text
                                    style={{
                                        color: '#333333',
                                        marginLeft: 10,
                                    }}
                                >
                                    {dataJam.total} Watt
                                </Text>
                            </View>
                        </View>
                    </Animatable.View>
                }
                {
                    showHari &&
                    <Animatable.View
                        animation="fadeIn"
                        style={{
                            marginHorizontal: 10,
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: '#ffffff',
                                borderWidth: 1,
                                borderColor: 'transparent',
                                marginTop: 10,
                                padding: 10,
                                shadowOffset: 2,
                                shadowColor: 'grey',
                                shadowOpacity: 2,
                                elevation: 2,
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 5,
                                }}
                            >
                                <View>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Detail Report {
                                        mcp2 === '0' ?
                                            'Semua MCP'
                                            :
                                            dataHari.report.filter((val) => val.mcp_id === mcp2).slice(0, 1).map((res) =>
                                                res.name
                                            )
                                    }</Text>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Tanggal {tanggalDari2} s/d {tanggalSampai2}</Text>
                                </View>
                                <View>
                                    {
                                        showDownload ?
                                            <ActivityIndicator />
                                            :
                                            <TouchableOpacity
                                                onPress={goDownload}
                                                style={{
                                                    backgroundColor: '#0095ef',
                                                    paddingVertical: 10,
                                                    paddingHorizontal: 15,
                                                    borderRadius: 5,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#ffffff',
                                                        fontSize: 15,
                                                    }}
                                                >Download</Text>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </View>
                            {
                                dataHari.report.map((val, index) =>
                                    <View key={index} style={{ marginTop: 5 }}>
                                        <View
                                            style={{
                                                flexDirection: 'row',

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Tanggal :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.jam}
                                            </Text>
                                        </View>
                                        {
                                            mcp2 === '0' ?
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                    }}
                                                >
                                                    <Text
                                                        style={{
                                                            color: '#666666',
                                                        }}
                                                    >
                                                        {val.name}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                        <View
                                            style={{
                                                marginLeft: 20,
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar R :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_r} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar S :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_s} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar T :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_t} Watt
                                                </Text>
                                            </View>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                backgroundColor: val.total_kwh === warna ? '#79ff4d' : '#ffffff',
                                                marginHorizontal: -10,
                                                paddingHorizontal: 10,

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Sub Total :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.total_kwh} Watt
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                height: 1,
                                                backgroundColor: '#666666',
                                                opacity: 0.4,
                                                marginVertical: 10,
                                            }}
                                        />
                                    </View>
                                )
                            }
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',

                                }}
                            >
                                <Text
                                    style={{
                                        color: '#333333',
                                    }}
                                >
                                    Total :
                                </Text>
                                <Text
                                    style={{
                                        color: '#333333',
                                        marginLeft: 10,
                                    }}
                                >
                                    {dataHari.total} Watt
                                </Text>
                            </View>
                        </View>
                    </Animatable.View>
                }
                {
                    showBulan &&
                    <Animatable.View
                        animation="fadeIn"
                        style={{
                            marginHorizontal: 10,
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: '#ffffff',
                                borderWidth: 1,
                                borderColor: 'transparent',
                                marginTop: 10,
                                padding: 10,
                                shadowOffset: 2,
                                shadowColor: 'grey',
                                shadowOpacity: 2,
                                elevation: 2,
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 5,
                                }}
                            >
                                <View>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Detail Report {
                                        mcp2 === '0' ?
                                            'Semua MCP'
                                            :
                                            dataBulan.report.filter((val) => val.mcp_id === mcp2).slice(0, 1).map((res) =>
                                                res.name
                                            )
                                    }</Text>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Bulan {
                                        bulanDari2 === bulanSampai2 ?
                                            moment(bulanDari2).format('MMMM')
                                            :
                                            moment(bulanDari2).format('MMMM') + ' s/d ' + moment(bulanSampai2).format('MMMM')
                                    }</Text>
                                </View>
                                <View>
                                    {
                                        showDownload ?
                                            <ActivityIndicator />
                                            :
                                            <TouchableOpacity
                                                onPress={goDownload}
                                                style={{
                                                    backgroundColor: '#0095ef',
                                                    paddingVertical: 10,
                                                    paddingHorizontal: 15,
                                                    borderRadius: 5,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#ffffff',
                                                        fontSize: 15,
                                                    }}
                                                >Download</Text>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </View>
                            {
                                dataBulan.report.map((val, index) =>
                                    <View key={index} style={{ marginTop: 5 }}>
                                        <View
                                            style={{
                                                flexDirection: 'row',

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Bulan :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.jam}
                                            </Text>
                                        </View>
                                        {
                                            mcp2 === '0' ?
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                    }}
                                                >
                                                    <Text
                                                        style={{
                                                            color: '#666666',
                                                        }}
                                                    >
                                                        {val.name}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                        <View
                                            style={{
                                                marginLeft: 20,
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar R :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_r} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar S :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_s} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar T :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_t} Watt
                                                </Text>
                                            </View>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                backgroundColor: val.total_kwh === warna ? '#79ff4d' : '#ffffff',
                                                marginHorizontal: -10,
                                                paddingHorizontal: 10,

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Sub Total :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.total_kwh} Watt
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                height: 1,
                                                backgroundColor: '#666666',
                                                opacity: 0.4,
                                                marginVertical: 10,
                                            }}
                                        />
                                    </View>
                                )
                            }
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',

                                }}
                            >
                                <Text
                                    style={{
                                        color: '#333333',
                                    }}
                                >
                                    Total :
                                </Text>
                                <Text
                                    style={{
                                        color: '#333333',
                                        marginLeft: 10,
                                    }}
                                >
                                    {dataBulan.total} Watt
                                </Text>
                            </View>
                        </View>
                    </Animatable.View>
                }
                {
                    showTahun &&
                    <Animatable.View
                        animation="fadeIn"
                        style={{
                            marginHorizontal: 10,
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: '#ffffff',
                                borderWidth: 1,
                                borderColor: 'transparent',
                                marginTop: 10,
                                padding: 10,
                                shadowOffset: 2,
                                shadowColor: 'grey',
                                shadowOpacity: 2,
                                elevation: 2,
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginBottom: 5,
                                }}
                            >
                                <View>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Detail Report {
                                        mcp2 === '0' ?
                                            'Semua MCP'
                                            :
                                            dataTahun.report.filter((val) => val.mcp_id === mcp2).slice(0, 1).map((res) =>
                                                res.name
                                            )
                                    }</Text>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#333333' }}>Tahun {
                                        tahunDari2 === tahunSampai2 ?
                                            tahunDari2
                                            :
                                            tahunDari2 + ' s/d ' + tahunSampai2
                                    }</Text>
                                </View>
                                <View>
                                    {
                                        showDownload ?
                                            <ActivityIndicator />
                                            :
                                            <TouchableOpacity
                                                onPress={goDownload}
                                                style={{
                                                    backgroundColor: '#0095ef',
                                                    paddingVertical: 10,
                                                    paddingHorizontal: 15,
                                                    borderRadius: 5,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#ffffff',
                                                        fontSize: 15,
                                                    }}
                                                >Download</Text>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </View>
                            {
                                dataTahun.report.map((val, index) =>
                                    <View key={index} style={{ marginTop: 5 }}>
                                        <View
                                            style={{
                                                flexDirection: 'row',

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Tahun :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.jam}
                                            </Text>
                                        </View>
                                        {
                                            mcp2 === '0' ?
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                    }}
                                                >
                                                    <Text
                                                        style={{
                                                            color: '#666666',
                                                        }}
                                                    >
                                                        {val.name}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                        <View
                                            style={{
                                                marginLeft: 20,
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar R :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_r} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar S :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_s} Watt
                                                </Text>
                                            </View>
                                            <View
                                                style={{
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between',

                                                }}
                                            >
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                    }}
                                                >
                                                    Saklar T :
                                                </Text>
                                                <Text
                                                    style={{
                                                        color: '#666666',
                                                        marginLeft: 10,
                                                    }}
                                                >
                                                    {val.total_kwh_t} Watt
                                                </Text>
                                            </View>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                backgroundColor: val.total_kwh === warna ? '#79ff4d' : '#ffffff',
                                                marginHorizontal: -10,
                                                paddingHorizontal: 10,

                                            }}
                                        >
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                }}
                                            >
                                                Sub Total :
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#666666',
                                                    marginLeft: 10,
                                                }}
                                            >
                                                {val.total_kwh} Watt
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                height: 1,
                                                backgroundColor: '#666666',
                                                opacity: 0.4,
                                                marginVertical: 10,
                                            }}
                                        />
                                    </View>
                                )
                            }
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',

                                }}
                            >
                                <Text
                                    style={{
                                        color: '#333333',
                                    }}
                                >
                                    Total :
                                </Text>
                                <Text
                                    style={{
                                        color: '#333333',
                                        marginLeft: 10,
                                    }}
                                >
                                    {dataTahun.total} Watt
                                </Text>
                            </View>
                        </View>
                    </Animatable.View>
                }
            </ScrollView>
            <Toast />
        </View>
    );
};

export default Report;
