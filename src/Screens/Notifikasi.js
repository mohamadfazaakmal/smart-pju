/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    FlatList,
    RefreshControl,
    ActivityIndicator,
    Alert,
    TextInput,
} from 'react-native';
import { Menu, MenuOption, MenuOptions, MenuTrigger } from 'react-native-popup-menu';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import Header from '../Components/Header';
import { useDispatch, useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getNotif, notifSelector } from '../Store/Actions/Notifikasi';
import { navigationRef } from '../Navigations/Utils';
import { Api } from '../Services/Api';
import axios from 'axios';
import { Config } from '../Config';
import PutNotif from '../Store/Actions/Notifikasi/PutNotif';
import Toast from 'react-native-toast-message';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import 'moment/locale/id';

const Notifikasi = ({ navigation }) => {

    const dispatch = useDispatch();
    const dataNotifAll = useSelector(notifSelector.selectAll);
    const dataNotif = dataNotifAll.filter((val) => val.isUnRead === true);
    const [token, setToken] = useState('');
    const [refreshing, setRefreshing] = useState(false);
    const [loading, setLoading] = useState(false);
    const [tab, setTab] = useState({
        belumDibaca: true,
        histori: false,
    });

    const getData = async () => {
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);
        setToken(val.accessToken);

        await dispatch(getNotif(val.accessToken)).unwrap();
        setRefreshing(false);
        setLoading(false);
        console.log(dataNotif);
    };

    // useEffect(() => {
    //     getData();
    // }, [dispatch]);

    useEffect(() => {
        const focusHandler = navigation.addListener('focus', () => {
            setLoading(true);
            getData();
            //ToastAndroid.show('Refresh..', ToastAndroid.SHORT);
        });
        return focusHandler;
    }, [navigation]);

    const onRefresh = () => {
        //Clear old data of the list
        //setDataSource([]);
        //Call the Service to get the latest data
        setRefreshing(true);
        getData();
    };

    const keluar = async () => {
        try {
            //const setup = '192.168.5.111';
            await AsyncStorage.removeItem('token');
            await AsyncStorage.removeItem('ping');
            navigation.navigate('Auth');
            Toast.show({
                type: 'success',
                text1: 'Berhasil',
                text2: 'Anda telah logout!',
                position: 'top',
                visibilityTime: 5000,
            });
        } catch (e) {
            Toast.show({
                type: 'error',
                text1: 'Maaf',
                text2: 'Logout gagal, silahkan coba lagi!',
                position: 'top',
                visibilityTime: 5000,
            });
        }
    };

    const ItemView = ({ item }) => {

        const detail = async () => {
            console.log(item);
            const baseQeury = Config.API_URL;
            var axios = require('axios');

            var config = {
                method: 'put',
                url: `${baseQeury}/notifications/${item.id}`,
                headers: {
                    'Authorization': 'Bearer ' + token,
                },
            };

            axios(config)
                .then(function (response) {
                    console.log(JSON.stringify(response.data));
                    navigationRef.navigate('Detail', { id: item.mcp_id, pilihan: 'NOTIF' });
                })
                .catch(function (error) {
                    console.log(error);
                });
            // console.log(baseQeury + item.id + token);
            // await axios.put(`${baseQeury}/notifications/${item.id}`, { headers: { Authorization: 'Bearer ' + token } })
            //     .then((res) => {
            //         console.log(res);
            //         navigationRef.navigate('Detail', { id: item.mcp_id, pilihan: 'NOTIF' });
            //     })
            //     .catch((error) => console.log(error));
        };

        return (
            <Animatable.View key={item.id} animation="fadeInUpBig">
                <TouchableOpacity style={{
                    backgroundColor: '#ffffff',
                    borderRadius: 5,
                    marginVertical: 2,
                    marginHorizontal: 5,
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 10,
                    shadowColor: '#000000',
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 5,
                }} onPress={detail}>
                    <View style={{ flex: 12 }}>
                        <Text style={styles.itemStyle}>
                            {item.body}
                        </Text>
                        <Text style={{ fontSize: 13, color: 'gray' }}>
                            {item.createdAt}
                        </Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <View style={{
                            height: 14,
                            width: 14,
                            backgroundColor: '#1eff00',
                            borderRadius: 7,
                            elevation: 2,
                        }} />
                    </View>
                </TouchableOpacity>
            </Animatable.View>
        );
    };

    if (loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                <ActivityIndicator size={'large'} color={'#0d73f0'} />
            </View>
        );
    }

    return (
        <View style={{ flex: 1 }}>
            <Animatable.View animation="fadeInDownBig" style={{ height: 100 }}>
                <LinearGradient
                    animation="fadeInUpBig"
                    colors={['#2F80ED', '#346eff', '#36D1DC']}
                    style={{ ...StyleSheet.absoluteFillObject, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
                    useAngle={true}
                    angle={150}
                    angleCenter={{ x: 0.5, y: 0.5 }}
                >
                    <View style={{ paddingHorizontal: 20, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 3 }}>
                            <View>
                                <TouchableOpacity
                                    style={{
                                        height: 30,
                                        width: 30,
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        borderColor: '#ffffff',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => navigation.goBack()}
                                >
                                    <Icons.ChevronLeftIcon size={20} color={'#ffffff'} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text
                                    style={{
                                        fontSize: 20,
                                        color: '#ffffff',
                                    }}
                                >
                                    Notifikasi
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginRight: 5 }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Notifikasi')}
                                    >
                                        <Icons.BellIcon size={30} color={'#ffffff'} />
                                        {
                                            dataNotif.length !== 0 ?
                                                <View
                                                    style={{
                                                        position: 'absolute',
                                                        backgroundColor: 'red',
                                                        width: 16,
                                                        height: 16,
                                                        borderRadius: 8,
                                                        right: 0,
                                                        top: 0,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                    }}>
                                                    <Text
                                                        style={{
                                                            color: 'white',
                                                            fontSize: 11,
                                                        }}>
                                                        {dataNotif.length}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Menu>
                                        <MenuTrigger>
                                            <Icons.DotsVerticalIcon size={30} color={'#ffffff'} />
                                        </MenuTrigger>
                                        <MenuOptions style={{ justifyContent: 'center' }}>
                                            <MenuOption onSelect={() => navigation.navigate('Profil')} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.UserIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Profil</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => navigation.navigate('Informasi')} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.InformationCircleIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Tentang Aplikasi</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => {
                                                Animated.timing(keluar(), {
                                                    duration: 300,
                                                    useNativeDriver: true,
                                                });
                                            }} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.LogoutIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Logout</Text>
                                                </View>
                                            </MenuOption>
                                        </MenuOptions>
                                    </Menu>
                                </View>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </Animatable.View>

            <Animatable.View
                style={{
                    flex: 1,
                    backgroundColor: '#e7edf7',
                }}
            >
                <Animatable.View
                    style={{
                        marginVertical: 20,
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            setTab({
                                belumDibaca: true,
                                histori: false,
                            });
                        }}
                        style={{
                            backgroundColor: tab.belumDibaca ? '#0073e6' : '#ffffff',
                            height: 35,
                            borderRadius: 10,
                            width: 150,
                            justifyContent: 'center',
                            alignItems: 'center',
                            shadowColor: '#000000',
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 16,
                                fontWeight: '500',
                                color: tab.belumDibaca ? '#ffffff' : '#666666',
                            }}
                        >Belum Dibaca</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            setTab({
                                belumDibaca: false,
                                histori: true,
                            });
                        }}
                        style={{
                            backgroundColor: tab.histori ? '#0073e6' : '#ffffff',
                            height: 35,
                            borderRadius: 10,
                            width: 150,
                            justifyContent: 'center',
                            alignItems: 'center',
                            shadowColor: '#000000',
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 16,
                                fontWeight: '500',
                                color: tab.histori ? '#ffffff' : '#666666',
                            }}
                        >Histori</Text>
                    </TouchableOpacity>
                </Animatable.View>

                {
                    tab.belumDibaca ?
                        <FlatList
                            data={dataNotif}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={ItemView}
                            showsVerticalScrollIndicator={false}
                            refreshControl={
                                <RefreshControl
                                    //refresh control used for the Pull to Refresh
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                />
                            }
                        />
                        :
                        <Histori />
                }
            </Animatable.View>
        </View>
    );
};

export default Notifikasi;

const Histori = () => {

    const dataNotifAll = useSelector(notifSelector.selectAll);
    const tgl = moment(new Date()).format('YYYY-MM-DD');
    const [tanggal, setTanggal] = useState(tgl);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [dataFilter, setDataFilter] = useState([]);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        let dates = moment(date).format('YYYY-MM-DD');
        setTanggal(dates);
        console.log('A date has been picked: ', dates);
        hideDatePicker();
    };

    useEffect(() => {
        let asw = moment(tanggal).format('DD/MM/YYYY');
        const dataNotif = async () => {
            const fil = await dataNotifAll.filter((val) => val.date.substring(0, 10) === asw);
            setDataFilter(fil);
            console.log(fil);
        };

        dataNotif();
    }, [tanggal]);

    const ItemView2 = ({ item }) => {
        return (
            <Animatable.View key={item.id} animation="fadeInUpBig">
                <TouchableOpacity style={{
                    backgroundColor: '#ffffff',
                    borderRadius: 5,
                    marginVertical: 2,
                    marginHorizontal: 5,
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingVertical: 20,
                    paddingHorizontal: 10,
                    shadowColor: '#000000',
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 5,
                }}>
                    <View style={{ flex: 12 }}>
                        <Text style={styles.itemStyle}>
                            {item.body}
                        </Text>
                        <Text style={{ fontSize: 13, color: 'gray' }}>
                            Jam {item.date.substring(12)}
                        </Text>
                    </View>
                </TouchableOpacity>
            </Animatable.View>
        );
    };

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View
                style={{
                    backgroundColor: '#ffffff',
                    borderWidth: 1,
                    borderColor: 'transparent',
                    marginHorizontal: 10,
                    shadowOffset: 2,
                    shadowColor: 'grey',
                    shadowOpacity: 2,
                    elevation: 2,
                }}
            >
                <View
                    style={{
                        backgroundColor: '#0095ef',
                        padding: 8,
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <Icons.FilterIcon size={20} color={'#ffffff'} />
                    <Text style={{ fontSize: 16, fontWeight: '500', color: '#ffffff', marginLeft: 5 }}>Filter Histori Notifikasi</Text>
                </View>
                <View
                    style={{ padding: 10 }}
                >
                    <View
                        style={{ marginBottom: 10 }}
                    >
                        <Text
                            style={{
                                color: '#708090',
                                fontSize: 15,
                            }}
                        >Pilih Tanggal</Text>
                    </View>
                    <View
                        style={{ flexDirection: 'row' }}
                    >
                        <TouchableOpacity
                            onPress={showDatePicker}
                            style={{
                                borderBottomLeftRadius: 5,
                                borderTopLeftRadius: 5,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: '#0073e6',
                                paddingHorizontal: 10,
                                height: 40,
                            }}
                        >
                            <Icons.CalendarIcon size={25} color={'#ffffff'} />
                        </TouchableOpacity>
                        <TextInput
                            style={{
                                flex: 1,
                                paddingVertical: 0,
                                color: '#666666',
                                backgroundColor: '#f2f2f2',
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 5,
                            }}
                            //onChangeText={jam}
                            value={tanggal}
                            placeholderTextColor="#666666"
                            selectionColor={'#666666'}
                            editable={false} />
                    </View>
                    {
                        dataFilter.length < 1 ?
                            <View
                                style={{ marginTop: 10 }}
                            >
                                <View
                                    style={{
                                        marginVertical: 10,
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            color: 'gray',
                                        }}
                                    >{moment(tanggal).format('dddd')}, {moment(tanggal).format('LL')}</Text>
                                </View>
                                <View>
                                    <Text
                                        style={{
                                            fontSize: 14,
                                            color: 'gray',
                                        }}
                                    >
                                        Tidak Ada Notifikasi Di Tanggal Ini, Silahkan Memilih Tanggal Lain.
                                    </Text>
                                </View>
                            </View>
                            :
                            <View
                                style={{ marginTop: 10 }}
                            >
                                <View
                                    style={{
                                        marginVertical: 10,
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            color: 'gray',
                                        }}
                                    >{moment(tanggal).format('dddd')}, {moment(tanggal).format('LL')} - {dataFilter.length} Notifikasi</Text>
                                </View>
                                {/* <FlatList
                                data={dataFilter}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={ItemView2}
                                showsVerticalScrollIndicator={false}
                            /> */}
                                {
                                    dataFilter.map((val, index) =>
                                        <Animatable.View key={index} animation="fadeInUpBig">
                                            <View key={index} style={{
                                                backgroundColor: '#ffffff',
                                                borderRadius: 5,
                                                marginVertical: 2,
                                                paddingVertical: 20,
                                                paddingHorizontal: 10,
                                                shadowColor: '#000000',
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 2,
                                                },
                                                shadowOpacity: 0.25,
                                                shadowRadius: 3.84,
                                                elevation: 5,
                                            }}>
                                                <View>
                                                    <Text style={styles.itemStyle}>
                                                        {val.body}
                                                    </Text>
                                                    <Text style={{ fontSize: 13, color: 'gray' }}>
                                                        Jam {val.date.substring(12)}
                                                    </Text>
                                                </View>

                                            </View>
                                        </Animatable.View>
                                    )
                                }
                            </View>
                    }
                </View>
                <DateTimePickerModal
                    date={new Date(tanggal)}
                    isVisible={isDatePickerVisible}
                    mode="date"
                    locale="id"
                    maximumDate={new Date()}
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                />
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        marginTop: 10,
    },
    itemStyle: {
        fontSize: 15,
        fontWeight: '500',
        color: '#666666',
    },
});
