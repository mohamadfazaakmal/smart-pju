/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, StatusBar } from 'react-native';

import * as Icons from 'react-native-heroicons/solid';
import Smartpju from '../Assets/images/misc/smartpju.svg';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ping from '../Services/Ping';
import { useDispatch } from 'react-redux';
import { getPing } from '../Store/Actions/Ping';
import Toast from 'react-native-toast-message';

const SplashScreen = ({ navigation }) => {
    const dispatch = useDispatch();

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem('ping');
            //console.log(value);
            if (value !== null) {
                dispatch(getPing(value))
                    .then((res) => {
                        navigation.navigate('Login');
                        //console.log(res);
                    })
                    .catch((e) => {
                        //console.log(e);
                        navigation.navigate('Setup');
                    });
            } else {
                navigation.navigate('Setup');
            }
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <Animatable.View
            animation="zoomIn" duration={1000}
            style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ffffff',
            }}>
            <View style={{ marginTop: 20, paddingTop: StatusBar.currentHeight }}>
                <Text
                    style={{
                        fontFamily: 'Inter-Bold',
                        fontWeight: 'bold',
                        fontSize: 30,
                        color: '#20315f',
                    }}>
                    SMART PJU
                </Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Smartpju
                    width={300}
                    height={300}
                />
            </View>
            <TouchableOpacity
                style={{
                    backgroundColor: '#2F80ED',
                    paddingHorizontal: 30,
                    paddingVertical: 20,
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}
                onPress={getData}>
                <Text
                    style={{
                        color: 'white',
                        fontSize: 20,
                        textAlign: 'center',
                        fontWeight: 'bold',
                        fontFamily: 'Roboto-MediumItalic',
                    }}>
                    Mulai Sekarang
                </Text>
                <Icons.ArrowRightIcon size={22} color={'#ffffff'} />
            </TouchableOpacity>
            <Toast />
        </Animatable.View>
    );
};

export default SplashScreen;
