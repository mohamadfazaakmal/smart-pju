/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    FlatList,
    RefreshControl,
    Alert,
} from 'react-native';
import { Menu, MenuOption, MenuOptions, MenuTrigger } from 'react-native-popup-menu';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { getNotif, notifSelector } from '../Store/Actions/Notifikasi';
import Toast from 'react-native-toast-message';

const Informasi = ({ navigation }) => {

    const dataNotifAll = useSelector(notifSelector.selectAll);
    const dataNotif = dataNotifAll.filter((val) => val.isUnRead === true);

    const keluar = async () => {
        try {
            //const setup = '192.168.5.111';
            await AsyncStorage.removeItem('token');
            //await AsyncStorage.removeItem('url');
            navigation.navigate('Auth');
            Toast.show({
                type: 'success',
                text1: 'Berhasil',
                text2: 'Anda telah logout!',
                position: 'top',
                visibilityTime: 5000,
            });
        } catch (e) {
            Toast.show({
                type: 'error',
                text1: 'Maaf',
                text2: 'Logout gagal, silahkan coba lagi!',
                position: 'top',
                visibilityTime: 5000,
            });
        }
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#e7edf7' }}>
            <Animatable.View animation="fadeInDownBig" style={{ height: 100 }}>
                <LinearGradient
                    animation="fadeInUpBig"
                    colors={['#2F80ED', '#346eff', '#36D1DC']}
                    style={{ ...StyleSheet.absoluteFillObject, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
                    useAngle={true}
                    angle={150}
                    angleCenter={{ x: 0.5, y: 0.5 }}
                >
                    <View style={{ paddingHorizontal: 20, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 3 }}>
                            <View>
                                <TouchableOpacity
                                    style={{
                                        height: 30,
                                        width: 30,
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        borderColor: '#ffffff',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => navigation.goBack()}
                                >
                                    <Icons.ChevronLeftIcon size={20} color={'#ffffff'} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text
                                    style={{
                                        fontSize: 20,
                                        color: '#ffffff',
                                    }}
                                >
                                    Tentang Aplikasi
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginRight: 5 }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Notifikasi')}
                                    >
                                        <Icons.BellIcon size={30} color={'#ffffff'} />
                                        {
                                            dataNotif.length !== 0 ?
                                                <View
                                                    style={{
                                                        position: 'absolute',
                                                        backgroundColor: 'red',
                                                        width: 16,
                                                        height: 16,
                                                        borderRadius: 8,
                                                        right: 0,
                                                        top: 0,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                    }}>
                                                    <Text
                                                        style={{
                                                            color: 'white',
                                                            fontSize: 11,
                                                        }}>
                                                        {dataNotif.length}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Menu>
                                        <MenuTrigger>
                                            <Icons.DotsVerticalIcon size={30} color={'#ffffff'} />
                                        </MenuTrigger>
                                        <MenuOptions style={{ justifyContent: 'center' }}>
                                            <MenuOption onSelect={() => navigation.navigate('Profil')} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.UserIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Profil</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => navigation.navigate('Informasi')} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.InformationCircleIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Tentang Aplikasi</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => {
                                                Animated.timing(keluar(), {
                                                    duration: 300,
                                                    useNativeDriver: true,
                                                });
                                            }} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.LogoutIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Logout</Text>
                                                </View>
                                            </MenuOption>
                                        </MenuOptions>
                                    </Menu>
                                </View>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </Animatable.View>
            <View style={{ backgroundColor: 'transparent', flex: 1, justifyContent: 'center', padding: 50 }}>
                <Text style={{ color: '#2F80ED', fontSize: 40, fontWeight: 'bold' }}>Smart PJU</Text>
                <View style={{ backgroundColor: '#2F80ED', height: 3, width: 150, marginVertical: 5 }} />
                <Text style={{ color: '#2F80ED', fontSize: 17, marginTop: 10 }}>Versi</Text>
                <Text style={{ color: '#2F80ED', fontSize: 22, fontWeight: 'bold', marginBottom: 20 }}>V 1.0.0</Text>
            </View>
        </View>
    );
};

export default Informasi;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        marginTop: 10,
    },
    itemStyle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
});
