/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    ToastAndroid,
    ActivityIndicator,
    FlatList,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { markers, mapDarkStyle, mapStandardStyle } from '../Assets/model/mapData';
import Header from '../Components/Header';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { getLocations, lokasiSelector } from '../Store/Actions/Locations';
import { navigationRef } from '../Navigations/Utils';
import { getMcps, mcpSelector } from '../Store/Actions/Mcp';

const { width, height } = Dimensions.get('window');
const CARD_HEIGHT = 150;
const CARD_WIDTH = width * 0.8;
const SPACING_FOR_CARD_INSET = width * 0.1 - 10;

const Home = ({ navigation }) => {

    const mapsData = useSelector(lokasiSelector.selectAll);
    const totalLokasi = useSelector(lokasiSelector.selectTotal);
    const dispatch = useDispatch();
    const mcpTotal = useSelector(mcpSelector.selectTotal);

    const initialMapState = {
        mapsData,
        region: {
            latitude: -6.970049417296218,
            longitude: 107.6495361328125,
            latitudeDelta: 0.199,
            longitudeDelta: 0.199,
        },
    };
    const [token, setToken] = useState('');
    const [loading, setLoading] = useState(true);

    const [state, setState] = React.useState(initialMapState);

    useEffect(() => {
        getData();
    }, [dispatch]);

    const getData = async () => {
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);
        setToken(val.accessToken);
        //console.log(value);
        await dispatch(getLocations(val.accessToken)).unwrap();
        await dispatch(getMcps(val.accessToken)).unwrap();
        //console.log(mapsData);
        setLoading(false);
    };

    useEffect(() => {
        const focusHandler = navigation.addListener('focus', () => {
            setLoading(true);
            getData();
            //ToastAndroid.show('Refresh..', ToastAndroid.SHORT);
        });
        return focusHandler;
    }, [navigation]);

    let mapIndex = 0;
    let mapAnimation = new Animated.Value(0);

    useEffect(() => {
        mapAnimation.addListener(({ value }) => {
            let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
            if (index >= mapsData.length) {
                index = mapsData.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }

            clearTimeout(regionTimeout);

            const regionTimeout = setTimeout(() => {
                if (mapIndex !== index) {
                    mapIndex = index;
                    const { latitude, longitude } = mapsData[index];
                    _map.current.animateToRegion(
                        {
                            latitude: Number(latitude),
                            longitude: Number(longitude),
                            latitudeDelta: state.region.latitudeDelta,
                            longitudeDelta: state.region.longitudeDelta,
                        },
                        350
                    );
                }
            }, 10);
        });
        //console.log(state.region);
    });

    const interpolations = mapsData.map((marker, index) => {
        const inputRange = [
            (index - 1) * CARD_WIDTH,
            index * CARD_WIDTH,
            ((index + 1) * CARD_WIDTH),
        ];

        const scale = mapAnimation.interpolate({
            inputRange,
            outputRange: [1, 1.5, 1],
            extrapolate: 'clamp',
        });

        return { scale };
    });

    const onMarkerPress = (mapEventData) => {
        const markerID = mapEventData._targetInst.return.key;

        let x = (markerID * CARD_WIDTH) + (markerID * 30);
        if (Platform.OS === 'ios') {
            x = x - SPACING_FOR_CARD_INSET;
        }

        _scrollView.current.scrollTo({ x: x, y: 0, animated: true });
    };

    const _map = React.useRef(null);
    const _scrollView = React.useRef(null);

    const getRegionForCoordinates = (points) => {
        // points should be an array of { latitude: X, longitude: Y }
        let minX, maxX, minY, maxY;
        //console.log('Points', points);
        // init first point
        ((point) => {
            minX = Number(point.latitude);
            maxX = Number(point.latitude);
            minY = Number(point.longitude);
            maxY = Number(point.longitude);
        })(points[0]);

        // calculate rect
        points.map((point) => {
            minX = Math.min(minX, Number(point.latitude));
            maxX = Math.max(maxX, Number(point.latitude));
            minY = Math.min(minY, Number(point.longitude));
            maxY = Math.max(maxY, Number(point.longitude));
        });

        const midX = (minX + maxX) / 2;
        const midY = (minY + maxY) / 2;
        const deltaX = (maxX - minX) + state.region.latitudeDelta;
        const deltaY = (maxY - minY) + state.region.longitudeDelta;

        return {
            latitude: midX,
            longitude: midY,
            latitudeDelta: deltaX,
            longitudeDelta: deltaY,
        };
    };

    if (loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                <ActivityIndicator size={'large'} color={'#0d73f0'} />
            </View>
        );
    }
    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#e7edf7' }}>
                <Header navigation={navigation} tinggi={130} judul={'Smart PJU'} />
                <Animatable.View animation="fadeInRightBig" style={{
                    flexDirection: 'row',
                    marginBottom: 5,
                    marginHorizontal: 20,
                    marginTop: -25,
                }}>
                    <View
                        style={{
                            flex: 1,
                            height: 60,
                            backgroundColor: '#00d3e0',
                            borderRadius: 10,
                            padding: 10,
                            justifyContent: 'center',
                            elevation: 2,
                            borderBottomColor: 'transparent',
                            borderBottomWidth: 2,
                        }}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={require('../Assets/images/location1.png')} style={{ width: 35, height: 35, tintColor: '#f4f5f8' }} />
                            <View style={{ alignItems: 'center', marginLeft: 5 }}>
                                <Text style={{ color: '#f4f5f8', fontSize: 15, fontWeight: '500' }}>Jumlah Lokasi</Text>
                                <Text style={{ color: '#f4f5f8', fontSize: 18, fontWeight: '500' }}>{totalLokasi}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: 5 }}></View>
                    <View
                        style={{
                            flex: 1,
                            height: 60,
                            backgroundColor: '#ffb74a',
                            borderRadius: 10,
                            padding: 10,
                            justifyContent: 'center',
                            elevation: 2,
                            borderBottomColor: 'transparent',
                            borderBottomWidth: 2,
                        }}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={require('../Assets/images/lightbulb.png')} style={{ width: 35, height: 35, tintColor: '#f4f5f8' }} />
                            <View style={{ alignItems: 'center', marginLeft: 5 }}>
                                <Text style={{ color: '#f4f5f8', fontSize: 15, fontWeight: '500' }}>Jumlah MCP</Text>
                                <Text style={{ color: '#f4f5f8', fontSize: 18, fontWeight: '500' }}>{mcpTotal}</Text>
                            </View>
                        </View>
                    </View>
                </Animatable.View>
                <Animatable.View
                    animation="fadeInUpBig"
                    style={{
                        flex: 1,
                        backgroundColor: '#e7edf7',
                    }}>
                    <View style={{ ...StyleSheet.absoluteFillObject }}>
                        <MapView
                            ref={_map}
                            region={getRegionForCoordinates(JSON.parse(JSON.stringify(mapsData)))}
                            style={{ ...StyleSheet.absoluteFillObject }}
                            provider={PROVIDER_GOOGLE}
                        >
                            {mapsData.map((marker, index) => {
                                //arrLatLon.push(marker.latitude, marker.longitude);
                                const scaleStyle = {
                                    transform: [
                                        {
                                            scale: interpolations[index].scale,
                                        },
                                    ],
                                };
                                return (
                                    <Marker key={index} coordinate={{
                                        latitude: Number(marker.latitude),
                                        longitude: Number(marker.longitude),
                                    }}
                                        onPress={(e) => onMarkerPress(e)}>
                                        <Animated.View style={[styles.markerWrap]}>
                                            <Animated.Image
                                                source={require('../Assets/images/map_marker3.png')}
                                                style={[styles.marker, scaleStyle]}
                                                resizeMode="cover"
                                            />
                                        </Animated.View>
                                    </Marker>
                                );
                            })}
                        </MapView>
                    </View>
                </Animatable.View>
                <Animated.FlatList
                    ref={_scrollView}
                    pagingEnabled
                    data={mapsData}
                    renderItem={({ item, index }) => (
                        <TabList item={item} />
                    )}
                    ItemSeparatorComponent={false}
                    keyExtractor={item => item.id}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={styles.scrollView}
                    scrollEventThrottle={1}
                    snapToInterval={CARD_WIDTH + 25}
                    snapToAlignment="center"
                    contentInset={{
                        top: 0,
                        left: SPACING_FOR_CARD_INSET,
                        bottom: 0,
                        right: SPACING_FOR_CARD_INSET,
                    }}
                    contentContainerStyle={{
                        paddingHorizontal: Platform.OS === 'android' ? SPACING_FOR_CARD_INSET : 0,
                    }}
                    onScroll={Animated.event(
                        [
                            {
                                nativeEvent: {
                                    contentOffset: {
                                        x: mapAnimation,
                                    },
                                },
                            },
                        ],
                        { useNativeDriver: true }
                    )}
                />
            </View>
        </>
    );
};

const TabList = ({ item }) => {
    //console.log(item);
    const [bt1, setBt1] = useState(0);
    return (
        <View style={styles.card}>
            <View style={styles.textContent}>
                <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                    <Image source={require('../Assets/images/location.png')} style={{ width: 20, height: 20, tintColor: '#00ffff' }} />
                    <Text numberOfLines={1} style={{
                        fontSize: 20,
                        color: '#373749',
                        // marginTop: 5,
                        fontWeight: '500',
                        marginLeft: 10,
                    }}>{item.name}</Text>
                </View>
                <View style={{ backgroundColor: '#adc1de', height: 2, borderRadius: 5, marginVertical: 5 }} />
                {
                    item.mcps.length > 0 ?
                        item.mcps.length > 1 ?
                            <CustomListAll data={item.mcps} />
                            :
                            item.mcps.map((mcp, index) =>
                                <CustomListOne data={mcp} key={index} />
                            )
                        :
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: '#ff8c00',
                                    fontWeight: '400',
                                }}
                            >
                                Tidak Ada MCP Di Lokasi Ini !
                            </Text>
                        </View>
                }
            </View>
        </View>
    );
};

const CustomListOne = ({ data }) => {

    //console.log(data.data);
    return (
        <View
            style={{
                paddingHorizontal: 5,
            }}
        >
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <TouchableOpacity disabled
                    style={{
                        backgroundColor: '#0d73f0', //bt1 === index ? '#0d73f0' : '#333333',
                        borderRadius: 5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingVertical: 5,
                        paddingHorizontal: 10,
                    }}
                    onPress={() => console.log()}
                >
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#ffffff',
                        }}
                    >
                        {data.name}
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                <Text numberOfLines={1} style={styles.cardtitle}>{data.name}</Text>
                <View style={styles.button}>
                    <TouchableOpacity
                        onPress={() => navigationRef.navigate('Detail', { id: data.id, pilihan: 'DETAIL' })}
                    >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={[styles.textSign, {
                                color: '#0d73f0',
                            }]}>Detail</Text>
                            <Icons.ChevronRightIcon size={15} color={'#0d73f0'} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            <View
                style={{
                    marginTop: 3,
                    flexDirection: 'row',
                }}
            >
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#333333',
                        }}
                    >
                        Total Energi
                    </Text>
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#333333',
                        }}
                    >
                        {data.data_sensor.energy} KWh
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#333333',
                        }}
                    >
                        Frekuensi
                    </Text>
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#333333',
                        }}
                    >
                        {data.data_sensor.freq} Hz
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#333333',
                        }}
                    >
                        Daya
                    </Text>
                    <Text
                        style={{
                            fontSize: 13,
                            color: '#333333',
                        }}
                    >
                        {data.data_sensor.total_watt} Watt
                    </Text>
                </View>
            </View>
        </View>
    );
};

const CustomListAll = ({ data }) => {

    const [dataTab, setDataTab] = useState(0);
    //console.log(data[0]);
    return (
        <View>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
                {
                    data.map((item, index) =>
                        <TouchableOpacity
                            style={{
                                backgroundColor: dataTab === index ? '#0d73f0' : '#333333',
                                borderRadius: 5,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                marginHorizontal: 5,
                            }}
                            onPress={() => setDataTab(index)}
                            key={item.id}
                        >
                            <Text
                                style={{
                                    fontSize: 13,
                                    color: '#ffffff',
                                }}
                            >
                                {item.name}
                            </Text>
                        </TouchableOpacity>
                    )
                }
            </View>

            <ViewAll item={data[dataTab]} />
        </View>
    );
};

const ViewAll = ({ item }) => {

    //console.log(item);
    return (
        <>
            {
                item.status ?
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                            <Text numberOfLines={1} style={styles.cardtitle}>{item.name}</Text>
                            <View style={styles.button}>
                                <TouchableOpacity
                                    onPress={() => navigationRef.navigate('Detail', { id: item.id })}
                                >
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={[styles.textSign, {
                                            color: '#0d73f0',
                                        }]}>Detail</Text>
                                        <Icons.ChevronRightIcon size={15} color={'#0d73f0'} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View
                            style={{
                                marginTop: 3,
                                flexDirection: 'row',
                            }}
                        >
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: '#333333',
                                    }}
                                >
                                    Total Energi
                                </Text>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: '#333333',
                                    }}
                                >
                                    {item.data_sensor.energy} KWh
                                </Text>
                            </View>
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: '#333333',
                                    }}
                                >
                                    Frekuensi
                                </Text>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: '#333333',
                                    }}
                                >
                                    {item.data_sensor.freq} Hz
                                </Text>
                            </View>
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: '#333333',
                                    }}
                                >
                                    Daya
                                </Text>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: '#333333',
                                    }}
                                >
                                    {item.data_sensor.total_watt} Watt
                                </Text>
                            </View>
                        </View>
                    </View>
                    :
                    <View
                        style={{
                            marginTop: 5,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 15,
                                color: '#ff8c00',
                                fontWeight: '400',
                            }}
                        >
                            {item.name} Tidak Ada Koneksi !
                        </Text>
                    </View>
            }
        </>
    );
};

export default Home;

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        marginTop: -100,
    },
    searchBox: {
        position: 'absolute',
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 5,
        padding: 10,
        shadowColor: '#cccccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    chipsScrollView: {
        position: 'absolute',
        top: Platform.OS === 'ios' ? 90 : 80,
        paddingHorizontal: 10,
    },
    chipsIcon: {
        marginRight: 5,
    },
    chipsItem: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        borderRadius: 20,
        padding: 8,
        paddingHorizontal: 20,
        marginHorizontal: 10,
        height: 35,
        shadowColor: '#cccccc',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10,
    },
    scrollView: {
        paddingVertical: 10,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        // padding: 10,
        elevation: 3,
        backgroundColor: '#ffffff',
        borderRadius: 10,
        marginRight: 10,
        shadowColor: '#000000',
        shadowRadius: 5,
        shadowOpacity: 0.4,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: 'hidden',
    },
    cardImage: {
        flex: 3,
        width: '100%',
        height: '100%',
        alignSelf: 'center',
    },
    textContent: {
        flex: 2,
        padding: 10,
    },
    cardtitle: {
        fontSize: 15,
        color: '#373749',
        // marginTop: 5,
        fontWeight: '500',
    },
    cardDescription: {
        fontSize: 12,
        color: '#444',
    },
    markerWrap: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
    },
    marker: {
        width: 30,
        height: 30,
    },
    button: {
        alignItems: 'flex-start',
        marginTop: 5,
    },
    signIn: {
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3,
    },
    textSign: {
        fontSize: 14,
        fontWeight: '500',
    },
});
