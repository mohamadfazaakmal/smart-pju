/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    ScrollView,
} from 'react-native';
import * as Icons from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';

import LoginSVG from '../Assets/images/misc/login.svg';
import GoogleSVG from '../Assets/images/misc/google.svg';
import FacebookSVG from '../Assets/images/misc/facebook.svg';
import TwitterSVG from '../Assets/images/misc/twitter.svg';

import CustomButton from '../Components/CustomButton';
import InputField from '../Components/InputField';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import PushNotification from 'react-native-push-notification';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import { getLogin, loginSelector } from '../Store/Actions/Login';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // install @hookform/resolvers (not @hookform/resolvers/yup)
import * as yup from 'yup';
import { pingSelector } from '../Store/Actions/Ping';

const schema = yup.object().shape({
    email: yup.string()
        .email('Email Tidak Valid')
        .required('Email Harus Diisi'),
    password: yup.string()
        .required('Password Harus Diisi'),
});

const data = {
    email: '',
    password: '',
};

const LoginScreen = ({ navigation }) => {

    const { control, handleSubmit, formState: { errors }, setValue, reset, getValues, watch } = useForm({
        resolver: yupResolver(schema),
        mode: 'onBlur',
        reValidateMode: 'onBlur',
    });

    const [securePassword, setSecurePassword] = useState(true);

    const dispatch = useDispatch();
    const [fcmToken, setFcmToken] = useState('');
    const { isFetching, isError, isLogin } = useSelector(loginSelector);
    //console.log(setupUrl);

    const signIn = async (value) => {
        console.log(value);
        let getPost = {
            email: value.email,
            password: value.password,
            fcm: fcmToken,
        };
        await dispatch(getLogin(getPost))
            .unwrap()
            .then(() => {
                navigation.navigate('Tab2');
                Toast.show({
                    type: 'success',
                    text1: 'Berhasil!',
                    text2: 'Anda Telah Login',
                    position: 'top',
                    visibilityTime: 5000,
                });//navigation.navigate('Login');
            })
            .catch(() => {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf',
                    text2: 'Login gagal, silahkan coba lagi!',
                    position: 'top',
                    visibilityTime: 5000,
                });
            });
    };

    useEffect(() => {
        const getToken = async () => {
            const data = await AsyncStorage.getItem('token');
            //console.log(data);
            if (data) {
                navigation.navigate('Tab2');
                console.log(data);
            } else {
                console.log('Tidak Ada Token');
            }
        };

        getToken();
    }, []);

    useEffect(() => {
        const getData = async () => {
            try {
                const value = await AsyncStorage.getItem('fcmToken');
                if (value !== null) {
                    setFcmToken(value);
                    console.log(value);
                }
            } catch (e) {
                console.log(e);
            }
        };

        getData();
    }, []);

    return (
        <Animatable.View style={{ flex: 1 }} animation="fadeIn" duration={1000}>
            <LinearGradient
                colors={['#2F80ED', '#5B86E5', '#36D1DC']}
                style={{ flex: 1 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ paddingHorizontal: 25, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ alignItems: 'center', paddingVertical: 60 }}>
                            <Image source={require('../Assets/images/sign-in.png')} style={{ width: 150, height: 150 }} />
                            {/* <LoginSVG
                            height={300}
                            width={300}
                            style={{ transform: [{ rotate: '-5deg' }] }}
                        /> */}
                        </View>
                        <Text
                            style={{
                                fontFamily: 'Roboto-Medium',
                                fontSize: 30,
                                fontWeight: '500',
                                color: '#ffffff',
                                marginBottom: 30,
                                textAlign: 'center',
                            }}>
                            Silahkan Login
                        </Text>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.email ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.MailIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="email"
                                    defaultValue={data.email}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Email"
                                            keyboardType="email-address"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                        />
                                    )}
                                />
                            </View>
                            {errors?.email?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.email.message}</Text>
                            }
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.password ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.LockClosedIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="password"
                                    defaultValue={data.password}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Password"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                            secureTextEntry={securePassword}
                                        />
                                    )}
                                />
                                <TouchableOpacity onPress={() => setSecurePassword(!securePassword)}>
                                    {
                                        securePassword ?
                                            <Icons.EyeOffIcon size={20} color={'#ffffff'} />
                                            :
                                            <Icons.EyeIcon size={20} color={'#ffffff'} />
                                    }
                                </TouchableOpacity>
                            </View>
                            {errors?.password?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.password.message}</Text>
                            }
                        </View>
                        <View>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Lupa')}
                                >
                                    <Text style={{
                                        fontSize: 16,
                                        fontWeight: '500',
                                        color: '#ffffff',
                                        fontStyle: 'italic',
                                        textDecorationLine: 'underline',
                                    }}>
                                        Lupa Password ?
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={handleSubmit(signIn)}
                            style={{
                                backgroundColor: '#ffffff',
                                padding: 15,
                                borderRadius: 10,
                                marginTop: 20,
                                marginBottom: 100,
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontWeight: '700',
                                    fontSize: 20,
                                    color: '#000000',
                                }}>
                                Login
                            </Text>
                        </TouchableOpacity>

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15 }}>Belum punya akun?</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                                <Text style={{
                                    color: '#ffffff',
                                    fontWeight: 'bold',
                                    fontStyle: 'italic',
                                    textDecorationLine: 'underline',
                                    fontSize: 16,
                                }}> Daftar Sekarang</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <Toast />
            </LinearGradient>
        </Animatable.View>
    );
};

export default LoginScreen;
