/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    Image,
} from 'react-native';
import * as Icons from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';

import LoginSVG from '../Assets/images/misc/login.svg';
import GoogleSVG from '../Assets/images/misc/google.svg';
import FacebookSVG from '../Assets/images/misc/facebook.svg';
import TwitterSVG from '../Assets/images/misc/twitter.svg';

import CustomButton from '../Components/CustomButton';
import InputField from '../Components/InputField';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

const LoginScreen = ({ navigation }) => {

    const [data, setData] = useState({
        name: '',
        email: '',
        telp: '',
        alamat: '',
        password: '',
        konfirmasi: '',
        secureText: true,
        secureTextKonfir: true,
        checkInputName: true,
        checkInputEmail: true,
        checkInputTelp: true,
        checkInputAlamat: true,
        checkInputPassword: true,
        checkInputKonfir: true,
    });

    const masuk = () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;

        if (!data.name.trim()) {
            setData({
                ...data,
                checkInputName: false,
            });
            return;
        }

        if (!data.email.trim()) {
            setData({
                ...data,
                checkInputEmail: false,
            });
            return;
        } else {
            if (reg.test(data.email) === false) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf',
                    text2: 'Format email anda tidak sesuai',
                    position: 'top',
                    visibilityTime: 5000,
                });
                setData({
                    ...data,
                    checkInputEmail: false,
                });
                return;
            }
        }

        if (!data.telp.trim()) {
            setData({
                ...data,
                checkInputTelp: false,
            });
            return;
        }

        if (!data.alamat.trim()) {
            setData({
                ...data,
                checkInputAlamat: false,
            });
            return;
        }

        if (!data.password.trim()) {
            setData({
                ...data,
                checkInputPassword: false,
            });
            return;
        }

        if (!data.konfirmasi.trim()) {
            setData({
                ...data,
                checkInputKonfir: false,
            });
            return;
        }

        if (data.konfirmasi !== data.password) {
            Toast.show({
                type: 'error',
                text1: 'Maaf',
                text2: 'Konfirmasi password tidak sama dengan password',
                position: 'top',
                visibilityTime: 5000,
            });
            setData({
                ...data,
                checkInputKonfir: false,
            });
        } else {
            console.log(data);
            navigation.navigate('Login');
        }
    };

    return (
        <Animatable.View style={{ flex: 1 }} animation="fadeIn" duration={1000}>
            <LinearGradient
                colors={['#2F80ED', '#5B86E5', '#36D1DC']}
                style={{ flex: 1 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ flex: 1, paddingHorizontal: 25, paddingTop: StatusBar.currentHeight }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ alignItems: 'center', paddingVertical: 50 }}>
                            <Image source={require('../Assets/images/register.png')} style={{ width: 150, height: 150 }} />
                            {/* <LoginSVG
                            height={300}
                            width={300}
                            style={{ transform: [{ rotate: '-5deg' }] }}
                        /> */}
                        </View>
                        <View style={{ paddingTop: 0 }}>
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Medium',
                                    fontSize: 30,
                                    fontWeight: '500',
                                    color: '#ffffff',
                                    marginBottom: 30,
                                    textAlign: 'center',
                                }}>
                                Silahkan Daftar
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputName ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 25,
                            }}>
                            <Icons.UserCircleIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.name}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        setData({
                                            ...data,
                                            name: val,
                                            checkInputName: true,
                                        });
                                    } else {
                                        setData({
                                            ...data,
                                            name: val,
                                            checkInputName: false,
                                        });
                                    }
                                }}
                                placeholder="Nama"
                                keyboardType="default"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                placeholderTextColor="#ffffff"
                                selectionColor={'#ffffff'}
                                maxFontSizeMultiplier={15}
                            />
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputEmail ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 25,
                            }}>
                            <Icons.MailIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.email}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        setData({
                                            ...data,
                                            email: val,
                                            checkInputEmail: true,
                                        });
                                    } else {
                                        setData({
                                            ...data,
                                            email: val,
                                            checkInputEmail: false,
                                        });
                                    }
                                }}
                                placeholder="Email"
                                keyboardType="email-address"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                placeholderTextColor="#ffffff"
                                selectionColor={'#ffffff'}
                            />
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputTelp ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 25,
                            }}>
                            <Icons.PhoneIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.telp}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        let num = val.replace('.', '').replace(' ', '');
                                        if (isNaN(num)) {
                                            console.log('errpr');
                                        } else {
                                            console.log(num);
                                            setData({
                                                ...data,
                                                telp: num,
                                                checkInputTelp: true,
                                            });
                                        }
                                    } else {
                                        setData({
                                            ...data,
                                            telp: val,
                                            checkInputTelp: false,
                                        });
                                    }
                                }}
                                placeholder="Telepon"
                                keyboardType="phone-pad"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                placeholderTextColor="#ffffff"
                                selectionColor={'#ffffff'}
                            />
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputPassword ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 25,
                            }}>
                            <Icons.LockClosedIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.password}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        setData({
                                            ...data,
                                            password: val,
                                            checkInputPassword: true,
                                        });
                                    } else {
                                        setData({
                                            ...data,
                                            password: val,
                                            checkInputPassword: false,
                                        });
                                    }
                                }}
                                placeholder="Password"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                secureTextEntry={data.secureText}
                                placeholderTextColor="#ffffff"
                                selectionColor={'#ffffff'}
                            />
                            <TouchableOpacity onPress={() => setData({
                                ...data,
                                secureText: !data.secureText,
                            })}>
                                {
                                    data.secureText ?
                                        <Icons.EyeOffIcon size={20} color={'#ffffff'} />
                                        :
                                        <Icons.EyeIcon size={20} color={'#ffffff'} />
                                }
                            </TouchableOpacity>
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputKonfir ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 25,
                            }}>
                            <Icons.LockClosedIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.konfirmasi}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        setData({
                                            ...data,
                                            konfirmasi: val,
                                            checkInputKonfir: true,
                                        });
                                    } else {
                                        setData({
                                            ...data,
                                            konfirmasi: val,
                                            checkInputKonfir: false,
                                        });
                                    }
                                }}
                                placeholder="Konfirmasi Password"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                secureTextEntry={data.secureTextKonfir}
                                placeholderTextColor="#ffffff"
                                selectionColor={'#ffffff'}
                            />
                            <TouchableOpacity onPress={() => setData({
                                ...data,
                                secureTextKonfir: !data.secureTextKonfir,
                            })}>
                                {
                                    data.secureTextKonfir ?
                                        <Icons.EyeOffIcon size={20} color={'#ffffff'} />
                                        :
                                        <Icons.EyeIcon size={20} color={'#ffffff'} />
                                }
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity
                            onPress={masuk}
                            style={{
                                backgroundColor: '#ffffff',
                                padding: 15,
                                borderRadius: 10,
                                marginBottom: 70,
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontWeight: '700',
                                    fontSize: 20,
                                    color: '#000000',
                                }}>
                                Daftar
                            </Text>
                        </TouchableOpacity>

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                                marginVertical: 30,
                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15 }}>Sudah punya akun?</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                <Text style={{
                                    color: '#ffffff',
                                    fontWeight: 'bold',
                                    fontStyle: 'italic',
                                    textDecorationLine: 'underline',
                                    fontSize: 16,
                                }}> Login Sekarang</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <Toast />
            </LinearGradient>
        </Animatable.View>
    );
};

export default LoginScreen;
