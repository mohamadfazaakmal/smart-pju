/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
    Image,
    ScrollView,
} from 'react-native';
import * as Icons from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';

import LoginSVG from '../Assets/images/misc/login.svg';
import GoogleSVG from '../Assets/images/misc/google.svg';
import FacebookSVG from '../Assets/images/misc/facebook.svg';
import TwitterSVG from '../Assets/images/misc/twitter.svg';

import CustomButton from '../Components/CustomButton';
import InputField from '../Components/InputField';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import PushNotification from 'react-native-push-notification';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import { getLogin, loginSelector } from '../Store/Actions/Login';

const LoginScreen = ({ navigation }) => {

    const [data, setData] = useState({
        email: 'marktelrnd@gmail.com',
        password: 'marktel123456',
        secureText: true,
        checkInputEmail: true,
        checkInputPassword: true,
    });
    const dispatch = useDispatch();
    const [fcmToken, setFcmToken] = useState('');
    const { isFetching, isError, isLogin } = useSelector(loginSelector);

    const masuk = async () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (!data.email.trim()) {
            setData({
                ...data,
                checkInputEmail: false,
            });
            return;
        } else {
            if (reg.test(data.email) === false) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf',
                    text2: 'Format email anda tidak sesuai',
                    position: 'top',
                    visibilityTime: 5000,
                });
                setData({
                    ...data,
                    checkInputEmail: false,
                });
                return;
            }
        }

        if (!data.password.trim()) {
            setData({
                ...data,
                checkInputPassword: false,
            });
            return;
        }

        //console.log(fcmToken);
        await signIn(data.email, data.password, fcmToken);

    };

    const signIn = async (email, password, fcm) => {
        let getPost = {
            email: email,
            password: password,
            fcm: fcm,
        };
        await dispatch(getLogin(getPost))
            .unwrap()
            .then(() => {
                navigation.navigate('Tab2');
            })
            .catch(() => {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf',
                    text2: 'Login gagal, silahkan coba lagi!',
                    position: 'top',
                    visibilityTime: 5000,
                });
            });
    };

    // useEffect(() => {
    //     if (isError) {
    //         Toast.show({
    //             type: 'error',
    //             text1: 'Maaf',
    //             text2: 'Login gagal, silahkan coba lagi!',
    //             position: 'top',
    //             visibilityTime: 2000,
    //         });
    //     }
    //     if (isLogin) {
    //         navigation.navigate('Tab');
    //     }
    // }, [isError, isLogin]);

    useEffect(() => {
        const getData = async () => {
            try {
                const value = await AsyncStorage.getItem('fcmToken');
                if (value !== null) {
                    setFcmToken(value);
                    console.log(value);
                }
            } catch (e) {
                console.log(e);
            }
        };

        getData();
    }, []);

    return (
        <Animatable.View style={{ flex: 1 }} animation="fadeIn" duration={1000}>
            <LinearGradient
                colors={['#2F80ED', '#5B86E5', '#36D1DC']}
                style={{ flex: 1 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ paddingHorizontal: 25, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ alignItems: 'center', paddingVertical: 60 }}>
                            <Image source={require('../Assets/images/sign-in.png')} style={{ width: 150, height: 150 }} />
                            {/* <LoginSVG
                            height={300}
                            width={300}
                            style={{ transform: [{ rotate: '-5deg' }] }}
                        /> */}
                        </View>
                        <Text
                            style={{
                                fontFamily: 'Roboto-Medium',
                                fontSize: 30,
                                fontWeight: '500',
                                color: '#ffffff',
                                marginBottom: 30,
                                textAlign: 'center',
                            }}>
                            Silahkan Login
                        </Text>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputEmail ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 25,
                            }}>
                            <Icons.MailIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.email}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        setData({
                                            ...data,
                                            email: val,
                                            checkInputEmail: true,
                                        });
                                    } else {
                                        setData({
                                            ...data,
                                            email: val,
                                            checkInputEmail: false,
                                        });
                                    }
                                }}
                                placeholder="Email"
                                keyboardType="email-address"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                placeholderTextColor="#ffffff"
                            />
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                borderBottomColor: data.checkInputPassword ? '#ffffff' : '#c72020',
                                borderBottomWidth: 1,
                                paddingBottom: 8,
                                marginBottom: 20,
                            }}>
                            <Icons.LockClosedIcon size={20} color={'#ffffff'} />
                            <TextInput
                                value={data.password}
                                onChangeText={(val) => {
                                    if (val.trim()) {
                                        setData({
                                            ...data,
                                            password: val,
                                            checkInputPassword: true,
                                        });
                                    } else {
                                        setData({
                                            ...data,
                                            password: val,
                                            checkInputPassword: false,
                                        });
                                    }
                                }}
                                placeholder="Password"
                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                secureTextEntry={data.secureText}
                                placeholderTextColor="#ffffff"
                            />
                            <TouchableOpacity onPress={() => setData({
                                ...data,
                                secureText: !data.secureText,
                            })}>
                                {
                                    data.secureText ?
                                        <Icons.EyeOffIcon size={20} color={'#ffffff'} />
                                        :
                                        <Icons.EyeIcon size={20} color={'#ffffff'} />
                                }
                            </TouchableOpacity>
                        </View>
                        <View>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <TouchableOpacity>
                                    <Text style={{
                                        fontSize: 16,
                                        fontWeight: '500',
                                        color: '#ffffff',
                                        fontStyle: 'italic',
                                        textDecorationLine: 'underline',
                                    }}>
                                        Lupa Password ?
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={masuk}
                            style={{
                                backgroundColor: '#ffffff',
                                padding: 15,
                                borderRadius: 10,
                                marginTop: 20,
                                marginBottom: 100,
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontWeight: '700',
                                    fontSize: 20,
                                    color: '#000000',
                                }}>
                                Login
                            </Text>
                        </TouchableOpacity>

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15 }}>Belum punya akun?</Text>
                            <TouchableOpacity onPress={() => {
                                navigation.navigate('Register');
                                setData({
                                    email: '',
                                    password: '',
                                    secureText: true,
                                    checkInputEmail: true,
                                    checkInputPassword: true,
                                });
                            }}>
                                <Text style={{
                                    color: '#ffffff',
                                    fontWeight: 'bold',
                                    fontStyle: 'italic',
                                    textDecorationLine: 'underline',
                                    fontSize: 16,
                                }}> Daftar Sekarang</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <Toast />
            </LinearGradient>
        </Animatable.View>
    );
};

export default LoginScreen;
