/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    FlatList,
    RefreshControl,
    Alert,
    PermissionsAndroid,
} from 'react-native';
import { Menu, MenuOption, MenuOptions, MenuTrigger } from 'react-native-popup-menu';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { getNotif, notifSelector } from '../Store/Actions/Notifikasi';
import Toast from 'react-native-toast-message';
import Pdf from 'react-native-pdf';
import ReactNativeBlobUtil from 'react-native-blob-util';
import RNFetchBlob from 'rn-fetch-blob';

const ViewPdf = ({ navigation, route }) => {

    const { url, filename } = route.params;
    const dataNotifAll = useSelector(notifSelector.selectAll);
    const dataNotif = dataNotifAll.filter((val) => val.isUnRead === true);

    const download = async () => {
        if (Platform.OS === 'android') {
            await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            );
            await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            );
        }
        let dirs = RNFetchBlob.fs.dirs;
        console.log(dirs.DownloadDir);
        RNFetchBlob.config({
            fileCache: true,
            appendExt: 'pdf',
            path: dirs.DownloadDir + '/' + filename + '.pdf',
            addAndroidDownloads: {
                //useDownloadManager: true,
                notification: true,
                title: filename + '.pdf',
                description: 'Download PDF Laporan KWH Meter Smart PJU Report',
                mime: 'application/pdf',
                mediaScannable: true,
                //path: dirs.DocumentDir + '/' + filename + '.pdf',
            },
        })
            .fetch(
                'GET',
                url,
            )
            .then((res) => {
                console.log('The file saved to ', res.path());
                Toast.show({
                    type: 'success',
                    text1: 'Berhasil !',
                    text2: 'Data Report Telah Di Download',
                    position: 'top',
                    visibilityTime: 5000,
                });
            })
            // Something went wrong:
            .catch((errorMessage, statusCode) => {
                console.log(errorMessage);
            });
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#e7edf7' }}>
            <Animatable.View animation="fadeInDownBig" style={{ height: 100 }}>
                <LinearGradient
                    animation="fadeInUpBig"
                    colors={['#2F80ED', '#346eff', '#36D1DC']}
                    style={{ ...StyleSheet.absoluteFillObject, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
                    useAngle={true}
                    angle={150}
                    angleCenter={{ x: 0.5, y: 0.5 }}
                >
                    <View style={{ paddingHorizontal: 20, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 3 }}>
                            <View>
                                <TouchableOpacity
                                    style={{
                                        height: 30,
                                        width: 30,
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        borderColor: '#ffffff',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => navigation.goBack()}
                                >
                                    <Icons.ChevronLeftIcon size={20} color={'#ffffff'} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text
                                    style={{
                                        fontSize: 20,
                                        color: '#ffffff',
                                    }}
                                >
                                    Download PDF | SmartPJU
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <TouchableOpacity
                                        onPress={download}
                                    >
                                        <Icons.DownloadIcon size={30} color={'#ffffff'} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </Animatable.View>
            <View style={styles.container}>
                <Pdf
                    trustAllCerts={false}
                    source={{
                        uri: url,
                        cache: false,
                    }}
                    onLoadComplete={(numberOfPages, filePath) => {
                        console.log(`Number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        console.log(`Current page: ${page}`);
                    }}
                    onError={error => {
                        console.log(error);
                    }}
                    onPressLink={uri => {
                        console.log(`Link pressed: ${uri}`);
                    }}
                    style={styles.pdf} />
            </View>
            <Toast />
        </View>
    );
};

export default ViewPdf;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 10,
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
});
