/* eslint-disable prettier/prettier */
/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    Image,
} from 'react-native';
import * as Icons from 'react-native-heroicons/solid';
import Toast from 'react-native-toast-message';

import LoginSVG from '../Assets/images/misc/login.svg';
import GoogleSVG from '../Assets/images/misc/google.svg';
import FacebookSVG from '../Assets/images/misc/facebook.svg';
import TwitterSVG from '../Assets/images/misc/twitter.svg';

import CustomButton from '../Components/CustomButton';
import InputField from '../Components/InputField';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup'; // install @hookform/resolvers (not @hookform/resolvers/yup)
import * as yup from 'yup';
import { postDaftar } from '../Store/Actions/Daftar';
import { Api } from '../Services/Api';

const schema = yup.object().shape({
    nama: yup.string()
        .required('Nama Harus Diisi'),
    email: yup.string()
        .email('Email Tidak Valid')
        .required('Email Harus Diisi'),
    alamat: yup.string()
        .required('Alamat Harus Diisi'),
    telp: yup.number().typeError('Telepon Harus Diisi Angka').transform((currentValue, originalValue) => {
        return typeof originalValue === 'string' && originalValue.trim() === '' ? undefined : currentValue;
    })
        .required('Telepon Harus Diisi'),
    password: yup.string()
        .min(6, 'Password Minimal 4 Karakter')
        .required('Password Harus Diisi'),
    konfirmasi: yup.string()
        .oneOf([
            yup.ref('password'),
            null,
        ], 'Konfirmasi Password Harus Sama Dengan Password')
        .required('Konfirmasi Password Harus Diisi'),
});

const data = {
    nama: '',
    email: '',
    telp: '',
    alamat: '',
    password: '',
    konfirmasi: '',
};

const RegisterScreen = ({ navigation }) => {

    const { control, handleSubmit, formState: { errors }, setValue, reset, getValues, watch } = useForm({
        resolver: yupResolver(schema),
        mode: 'onBlur',
        reValidateMode: 'onBlur',
    });

    const [securePassword, setSecurePassword] = useState(true);
    const [secureKonfirmasi, setSecureKonfirmasi] = useState(true);

    //console.log(errors);

    const masuk = async (value) => {
        //let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        //reg.test(data.email) === false
        console.log(value);
        // let getPost = {
        //     nama: value.nama,
        //     email: value.email,
        //     password: value.password,
        //     telp: value.telp,
        //     alamat: value.alamat,
        // };
        await Api.post('/register', {
            name: value.nama,
            email: value.email,
            password: value.password,
            handphone: value.telp,
            address: value.alamat,
        })
            .then((res) => {
                let row = res.data;
                console.log(row);
                if (row.status) {
                    navigation.navigate('Login');
                    Toast.show({
                        type: 'success',
                        text1: 'Berhasil!',
                        text2: row.message,
                        position: 'top',
                        visibilityTime: 5000,
                    });
                } else {
                    //console.log(res.data);
                    Toast.show({
                        type: 'error',
                        text1: 'Maaf!',
                        text2: row.message,
                        position: 'top',
                        visibilityTime: 5000,
                    });
                    //navigation.navigate('Login');
                }
            })
            .catch((e) => {
                console.log(e);
                Toast.show({
                    type: 'error',
                    text1: 'Maaf!',
                    text2: 'Registrasi gagal, silahkan coba lagi',
                    position: 'top',
                    visibilityTime: 5000,
                });
            });
        //navigation.navigate('Login');
    };

    return (
        <Animatable.View style={{ flex: 1 }} animation="fadeIn" duration={1000}>
            <LinearGradient
                colors={['#2F80ED', '#5B86E5', '#36D1DC']}
                style={{ flex: 1 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ flex: 1, paddingHorizontal: 25, paddingTop: StatusBar.currentHeight }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ alignItems: 'center', paddingVertical: 50 }}>
                            <Image source={require('../Assets/images/register.png')} style={{ width: 150, height: 150 }} />
                            {/* <LoginSVG
                            height={300}
                            width={300}
                            style={{ transform: [{ rotate: '-5deg' }] }}
                        /> */}
                        </View>
                        <View style={{ paddingTop: 0 }}>
                            <Text
                                style={{
                                    fontFamily: 'Roboto-Medium',
                                    fontSize: 30,
                                    fontWeight: '500',
                                    color: '#ffffff',
                                    marginBottom: 30,
                                    textAlign: 'center',
                                }}>
                                Silahkan Daftar
                            </Text>
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.nama ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.UserCircleIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="nama"
                                    defaultValue={data.nama}
                                    render={({ field: { onChange, onBlur, value } }) => {
                                        return (
                                            <TextInput
                                                style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                                onBlur={onBlur}
                                                onChangeText={onChange}
                                                value={value}
                                                placeholder="Nama"
                                                placeholderTextColor="#ffffff"
                                                selectionColor={'#ffffff'}
                                            />
                                        );
                                    }}
                                />
                            </View>
                            {errors?.nama?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.nama.message}</Text>
                            }
                        </View>
                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.email ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.MailIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="email"
                                    defaultValue={data.email}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Email"
                                            keyboardType="email-address"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                        />
                                    )}
                                />
                            </View>
                            {errors?.email?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.email.message}</Text>
                            }
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.alamat ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.IdentificationIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="alamat"
                                    defaultValue={data.alamat}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Alamat"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                        />
                                    )}
                                />
                            </View>
                            {errors?.alamat?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.alamat.message}</Text>
                            }
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{

                                    flexDirection: 'row',
                                    borderBottomColor: errors?.telp ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.PhoneIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="telp"
                                    defaultValue={data.telp}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Telepon"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                            keyboardType={'phone-pad'}
                                        />
                                    )}
                                />
                            </View>
                            {errors?.telp?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.telp.message}</Text>
                            }
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.password ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.LockClosedIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="password"
                                    defaultValue={data.password}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Password"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                            secureTextEntry={securePassword}
                                        />
                                    )}
                                />
                                <TouchableOpacity onPress={() => setSecurePassword(!securePassword)}>
                                    {
                                        securePassword ?
                                            <Icons.EyeOffIcon size={20} color={'#ffffff'} />
                                            :
                                            <Icons.EyeIcon size={20} color={'#ffffff'} />
                                    }
                                </TouchableOpacity>
                            </View>
                            {errors?.password?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.password.message}</Text>
                            }
                        </View>

                        <View
                            style={{
                                marginBottom: 25,
                            }}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderBottomColor: errors?.konfirmasi ? '#e60000' : '#ffffff',
                                    borderBottomWidth: 1,
                                    paddingBottom: 8,
                                    marginBottom: 8,
                                }}
                            >
                                <Icons.LockClosedIcon size={20} color={'#ffffff'} />
                                <Controller
                                    control={control}
                                    name="konfirmasi"
                                    defaultValue={data.konfirmasi}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput
                                            style={{ flex: 1, paddingVertical: 0, color: '#ffffff' }}
                                            onBlur={onBlur}
                                            onChangeText={onChange}
                                            value={value}
                                            placeholder="Konfirmasi Password"
                                            placeholderTextColor="#ffffff"
                                            selectionColor={'#ffffff'}
                                            secureTextEntry={secureKonfirmasi}
                                        />
                                    )}
                                />
                                <TouchableOpacity onPress={() => setSecureKonfirmasi(!secureKonfirmasi)}>
                                    {
                                        secureKonfirmasi ?
                                            <Icons.EyeOffIcon size={20} color={'#ffffff'} />
                                            :
                                            <Icons.EyeIcon size={20} color={'#ffffff'} />
                                    }
                                </TouchableOpacity>
                            </View>
                            {errors?.konfirmasi?.message &&
                                <Text style={{
                                    color: '#e60000',
                                    fontSize: 10,
                                    marginLeft: 6,
                                }}>{errors.konfirmasi.message}</Text>
                            }
                        </View>

                        <TouchableOpacity
                            onPress={handleSubmit(masuk)}
                            style={{
                                backgroundColor: '#ffffff',
                                padding: 15,
                                borderRadius: 10,
                                marginBottom: 70,
                            }}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontWeight: '700',
                                    fontSize: 20,
                                    color: '#000000',
                                }}>
                                Daftar
                            </Text>
                        </TouchableOpacity>

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                                marginVertical: 30,
                            }}>
                            <Text style={{ color: '#ffffff', fontSize: 15 }}>Sudah punya akun?</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                <Text style={{
                                    color: '#ffffff',
                                    fontWeight: 'bold',
                                    fontStyle: 'italic',
                                    textDecorationLine: 'underline',
                                    fontSize: 16,
                                }}> Login Sekarang</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <Toast />
            </LinearGradient>
        </Animatable.View>
    );
};

export default RegisterScreen;
