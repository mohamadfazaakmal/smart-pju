/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Animated,
    Dimensions,
    Platform,
    FlatList,
    RefreshControl,
    Alert,
    Switch,
    ActivityIndicator,
} from 'react-native';

import { Menu, MenuOption, MenuOptions, MenuTrigger } from 'react-native-popup-menu';
import CircularProgress from 'react-native-circular-progress-indicator';

import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import Header from '../Components/Header';
import { useDispatch, useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getNotif, notifSelector } from '../Store/Actions/Notifikasi';
import { getMcps, mcpSelector } from '../Store/Actions/Mcp';

import { VictoryAxis, VictoryChart, VictoryLine, VictoryScatter, VictoryTooltip, VictoryContainer, VictoryPie, VictoryBar } from 'victory-native';
import Material from '../Assets/model/Material';
import { energiSelector, getEnergiByMcp } from '../Store/Actions/Energi';
import SetSaklar from '../Store/Actions/Mcp/SetSaklar';
import Timeline from 'react-native-timeline-flatlist';
import Toast from 'react-native-toast-message';
import Loader from '../Components/Loader';

const Detail = ({ navigation, route }) => {

    const { id } = route.params;
    const { pilihan } = route.params;
    const dispatch = useDispatch();
    const dataNotifAll = useSelector(notifSelector.selectAll);
    const dataNotif = dataNotifAll.filter((val) => val.isUnRead === true);
    const dn = dataNotifAll.filter((val) => val.mcp_id === id).slice(0, 10);
    const dataDetail = useSelector((state) => mcpSelector.selectById(state, id));
    const dataChart = useSelector(energiSelector);

    const { width, height } = Dimensions.get('window');

    const [data, setData] = useState([]);

    const [menu, setMenu] = useState({});
    const [token, setToken] = useState('');

    const [jam, setJam] = useState([]);
    const [energi, setEnergi] = useState([]);
    const [loading, setLoading] = useState(false);
    const [selected, setSelected] = useState(null);
    //console.log(menu);

    useEffect(() => {
        getMenu();
    }, []);

    useEffect(() => {
        setLoading(true);
        getData();
        getTimeLine();
        getMcp();
    }, [dispatch]);

    const getMcp = async () => {
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);

        await dispatch(getMcps(val.accessToken))
            .unwrap()
            .then((res) => {
                setToken(val.accessToken);
                setLoading(false);
                console.log(res);
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const getData = async () => {
        const value = await AsyncStorage.getItem('token');
        let val = JSON.parse(value);

        let getPost = {
            AuthStr: val.accessToken, id: id,
        };

        await dispatch(getEnergiByMcp(getPost)).unwrap()
            .then((res) => {
                //console.log(res);
                let s = [];
                //let r = [];
                res.map((d) => {
                    let y2;
                    if (d.total_kwh < 0) {
                        y2 = 0;
                    } else {
                        y2 = d.total_kwh;
                    }
                    s.push({
                        x: d.jam,
                        y: Number(y2),
                    });
                    // r.push(d.total_kwh);
                });
                //console.log(s);
                //setJam(s);
                setEnergi(s);
                //setLoading(false);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const getTimeLine = () => {
        let t = [];
        dn.map((m) => {
            t.push({
                time: m.createdAt,
                title: m.title,
                description: m.body,
            });
            //console.log(t);
            setData(t);
        });
    };

    const getMenu = () => {
        switch (pilihan) {
            case 'DETAIL':
                setMenu({
                    detail: true,
                    chart: false,
                    notif: false,
                    nama: 'DETAIL',
                });
                break;
            case 'GRAFIK':
                setMenu({
                    detail: false,
                    chart: true,
                    notif: false,
                    nama: 'GRAFIK',
                });
                break;
            case 'NOTIF':
                setMenu({
                    detail: false,
                    chart: false,
                    notif: true,
                    nama: 'NOTIF',
                });
                break;
            default:
                setMenu({
                    detail: true,
                    chart: false,
                    notif: false,
                    nama: 'DETAIL',
                });
                break;
        }
    };

    const keluar = async () => {
        try {
            //const setup = '192.168.5.111';
            await AsyncStorage.removeItem('token');
            //await AsyncStorage.removeItem('url');
            navigation.navigate('Auth');
            Toast.show({
                type: 'success',
                text1: 'Berhasil',
                text2: 'Anda telah logout!',
                position: 'top',
                visibilityTime: 5000,
            });
        } catch (e) {
            Toast.show({
                type: 'error',
                text1: 'Maaf',
                text2: 'Logout gagal, silahkan coba lagi!',
                position: 'top',
                visibilityTime: 5000,
            });
        }
    };

    function onEventPress(data) {
        setSelected(data);
        console.log(data);
    }

    function renderSelected() {
        if (selected) { return <Text style={{ marginTop: 10 }}>{selected.title} !!! {selected.description}, {selected.time}</Text>; }
    }

    function renderDetail(rowData, sectionID, rowID) {
        let title = <Text style={{
            fontSize: 16,
            fontWeight: '500',
            color: rowData.title === 'Danger' ?
                'red'
                :
                rowData.title === 'Warning' ?
                    'orange'
                    :
                    'blue',
        }}>{rowData.title} !</Text>;
        var desc = null;
        if (rowData.description) {
            desc = (
                <View style={styles.descriptionContainer}>
                    <Text style={[styles.textDescription]}>{rowData.description}</Text>
                </View>
            );
        }

        return (
            <View style={{ flex: 1 }}>
                {title}
                {desc}
            </View>
        );
    }

    if (loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                <ActivityIndicator size={'large'} color={'#0d73f0'} />
            </View>
        );
    }
    //console.log(dn);
    return (
        <View style={{ flex: 1 }}>
            <Animatable.View animation="fadeInDownBig" style={{ height: 100 }}>
                <LinearGradient
                    animation="fadeInUpBig"
                    colors={['#2F80ED', '#346eff', '#36D1DC']}
                    style={{ ...StyleSheet.absoluteFillObject, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
                    useAngle={true}
                    angle={150}
                    angleCenter={{ x: 0.5, y: 0.5 }}
                >
                    <View style={{ paddingHorizontal: 20, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 3 }}>
                            <View>
                                <TouchableOpacity
                                    style={{
                                        height: 30,
                                        width: 30,
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        borderColor: '#ffffff',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => navigation.goBack()}
                                >
                                    <Icons.ChevronLeftIcon size={20} color={'#ffffff'} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text
                                    style={{
                                        fontSize: 20,
                                        color: '#ffffff',
                                    }}
                                >
                                    Detail {dataDetail.name}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginRight: 5 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            navigation.navigate('Notifikasi');
                                            setTimeout(() => {
                                                setMenu({
                                                    detail: false,
                                                    chart: false,
                                                    notif: true,
                                                    nama: 'NOTIF',
                                                });
                                            }, 500);
                                        }}
                                    >
                                        <Icons.BellIcon size={30} color={'#ffffff'} />
                                        {
                                            dataNotif.length !== 0 ?
                                                <View
                                                    style={{
                                                        position: 'absolute',
                                                        backgroundColor: 'red',
                                                        width: 16,
                                                        height: 16,
                                                        borderRadius: 8,
                                                        right: 0,
                                                        top: 0,
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                    }}>
                                                    <Text
                                                        style={{
                                                            color: 'white',
                                                            fontSize: 11,
                                                        }}>
                                                        {dataNotif.length}
                                                    </Text>
                                                </View>
                                                : null
                                        }
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Menu>
                                        <MenuTrigger>
                                            <Icons.DotsVerticalIcon size={30} color={'#ffffff'} />
                                        </MenuTrigger>
                                        <MenuOptions style={{ justifyContent: 'center' }}>
                                            <MenuOption onSelect={() => navigation.navigate('Profil')} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.UserIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Profil</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => navigation.navigate('Informasi')} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.InformationCircleIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Tentang Aplikasi</Text>
                                                </View>
                                            </MenuOption>
                                            <MenuOption onSelect={() => {
                                                Animated.timing(keluar(), {
                                                    duration: 300,
                                                    useNativeDriver: true,
                                                });
                                            }} >
                                                <View
                                                    style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >
                                                    <Icons.LogoutIcon size={25} color={'#666666'} />
                                                    <Text style={{ color: '#666666', marginLeft: 5 }}>Logout</Text>
                                                </View>
                                            </MenuOption>
                                        </MenuOptions>
                                    </Menu>
                                </View>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </Animatable.View>
            <Animatable.View
                style={{
                    flex: 1,
                    backgroundColor: '#e7edf7',
                }}
                animation="fadeIn"
            >
                <Animatable.View
                    animation={'fadeInRightBig'}
                    style={{
                        marginHorizontal: 40,
                        marginVertical: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#ffffff',
                        height: 35,
                        borderRadius: 10,
                        flexDirection: 'row',
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            padding: 3,
                            backgroundColor: 'transparent',
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                backgroundColor: menu.detail ? '#0073e6' : 'transparent',
                                borderRadius: menu.detail ? 10 : 0,
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}
                            onPress={() => {
                                setMenu({
                                    detail: true,
                                    chart: false,
                                    notif: false,
                                    nama: 'DETAIL',
                                });
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: menu.detail ? '#ffffff' : '#4d4d66',
                                }}
                            >
                                Detail
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            padding: 3,
                            backgroundColor: 'transparent',
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                backgroundColor: menu.chart ? '#0073e6' : 'transparent',
                                borderRadius: menu.chart ? 10 : 0,
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}
                            onPress={() => {
                                setMenu({
                                    detail: false,
                                    chart: true,
                                    notif: false,
                                    nama: 'GRAFIK',
                                });
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: menu.chart ? '#ffffff' : '#4d4d66',
                                }}
                            >
                                Grafik
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            padding: 3,
                            backgroundColor: 'transparent',
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                backgroundColor: menu.notif ? '#2F80ED' : 'transparent',
                                borderRadius: menu.notif ? 10 : 0,
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}
                            onPress={() => {
                                setMenu({
                                    detail: false,
                                    chart: false,
                                    notif: true,
                                    nama: 'NOTIF',
                                });
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: menu.notif ? '#ffffff' : '#4d4d66',
                                }}
                            >
                                Notifikasi
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Animatable.View>

                <View
                    style={{
                        flex: 1,
                    }}
                >
                    {
                        menu.detail ?
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <CustomFlatList item={dataDetail} token={token} />
                            </ScrollView>
                            : null
                    }
                    {
                        menu.chart ?
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View
                                    style={{
                                        backgroundColor: '#ffffff',
                                        borderRadius: 10,
                                        padding: 5,
                                        borderColor: '#00ffff',
                                        borderWidth: 1,
                                        elevation: 1,
                                        marginHorizontal: 10,
                                    }}
                                >
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                        }}
                                    >
                                        <Image source={require('../Assets/images/electricity.png')} style={{ width: 25, height: 25, tintColor: '#e6e600' }} />
                                        <Text
                                            style={{
                                                fontSize: 18,
                                                color: '#4d4d66',
                                                marginLeft: 5,
                                                alignSelf: 'flex-start',
                                            }}
                                        >
                                            Grafik Total Daya Yang Terpakai
                                        </Text>
                                    </View>
                                    <View
                                        style={{
                                            marginTop: 5,
                                        }}
                                    >
                                        <ScrollView horizontal>
                                            <VictoryChart
                                                theme={Material}
                                                domainPadding={5}
                                                height={400}
                                                width={energi.length > 5 ? width * 2 : width - 20}
                                            >

                                                <VictoryLine
                                                    style={{
                                                        data: {
                                                            stroke: '#0073e6',
                                                        },
                                                        parent: {
                                                            border: '1px solid #ffffff',
                                                        },
                                                    }}
                                                    data={energi}
                                                    categories={{ energi }}
                                                />
                                                <VictoryScatter
                                                    labels={({ datum }) => `Jam ${datum.x} - ${datum.y} watt`}
                                                    labelComponent={
                                                        <VictoryTooltip
                                                            constrainToVisibleArea
                                                            renderInPortal={false}
                                                            flyoutWidth={200}
                                                            flyoutHeight={40}
                                                            style={{ fill: '#c43a31' }}
                                                        />
                                                    }
                                                    data={energi}
                                                    size={7}
                                                    style={{
                                                        data: {
                                                            fill: '#0073e6',
                                                        },
                                                    }}
                                                />
                                                <VictoryAxis
                                                    style={{
                                                        grid: {
                                                            stroke: 'transparent',
                                                        },
                                                    }}
                                                />
                                                <VictoryAxis
                                                    dependentAxis
                                                    style={{
                                                        axis: {
                                                            stroke: 'transparent',
                                                        },
                                                        grid: {
                                                            stroke: 'grey',
                                                        },
                                                    }}
                                                />
                                            </VictoryChart>
                                        </ScrollView>
                                    </View>
                                    {/* <View>
                                        <ScrollView horizontal>
                                            <VictoryChart
                                                theme={Material}
                                                domainPadding={10}
                                                height={350}
                                                width={energi.length > 7 ? width * 2 : width - 20}
                                            >
                                                <VictoryBar
                                                    barWidth={25}
                                                    style={{ data: { fill: '' } }}
                                                    data={energi}
                                                    labels={({ datum }) => `Jam ${datum.x}, ${datum.y} kwh`}
                                                    labelComponent={
                                                        <VictoryTooltip
                                                            constrainToVisibleArea
                                                            renderInPortal={false}
                                                            flyoutWidth={200}
                                                            flyoutHeight={40}
                                                        />
                                                    }
                                                />
                                            </VictoryChart>
                                        </ScrollView>
                                    </View> */}
                                </View>
                            </ScrollView>
                            : null
                    }
                    {
                        menu.notif ?
                            <View
                                style={{
                                    backgroundColor: '#ffffff',
                                    borderRadius: 10,
                                    padding: 5,
                                    borderColor: '#00ffff',
                                    borderWidth: 1,
                                    elevation: 1,
                                    marginHorizontal: 10,
                                    marginVertical: 10,
                                    flex: 1,
                                }}
                            >
                                {renderSelected()}
                                <Timeline
                                    style={{
                                        marginTop: 10,
                                    }}

                                    data={data}
                                    circleSize={20}
                                    circleColor="rgb(45,156,219)"
                                    lineColor="rgb(45,156,219)"
                                    timeContainerStyle={{ minWidth: 130, marginTop: -2 }}
                                    timeStyle={{ textAlign: 'center', backgroundColor: '#ff9797', color: 'white', padding: 5, borderRadius: 13 }}
                                    descriptionStyle={{ color: 'gray' }}
                                    options={{
                                        style: { paddingTop: 5 },
                                        showsVerticalScrollIndicator: false,
                                    }}
                                    isUsingFlatlist={true}
                                    innerCircle={'dot'}
                                    onEventPress={onEventPress}
                                    renderDetail={renderDetail}
                                    separator={false}
                                    detailContainerStyle={{ marginBottom: 20, paddingHorizontal: 5, backgroundColor: '#BBDAFF', borderRadius: 10 }}
                                />
                            </View>
                            : null
                    }
                </View>
            </Animatable.View>
            <Toast />
        </View>
    );
};

const CustomFlatList = ({ item, token }) => {

    const [saklarR, setSaklarR] = useState(false);
    const [saklarS, setSaklarS] = useState(false);
    const [saklarT, setSaklarT] = useState(false);
    const [disable, setDisable] = useState(false);
    const dispatch = useDispatch();

    const toggleSwitchR = async () => {
        //setSaklarR(previousState => !previousState);
        setDisable(true);
        const newState = !saklarR;
        console.log(item.id, newState ? 'on' : 'off', saklarS ? 'on' : 'off', saklarT ? 'on' : 'off', token);
        await SetSaklar(item.id, newState ? 'on' : 'off', saklarS ? 'on' : 'off', saklarT ? 'on' : 'off', token)
            .then(async function (response) {
                setDisable(false);
                setSaklarR(newState);
                await dispatch(getMcps(token));
                // Toast.show({
                //     type: 'success',
                //     text1: 'Berhasil !',
                //     text2: 'Lampu Fasa R Telah ' + newState ? 'On' : 'Off',
                //     position: 'top',
                //     visibilityTime: 5000,
                // });
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf !',
                    text2: 'On Off Lampu Gagal',
                    position: 'top',
                    visibilityTime: 5000,
                });
                console.log(error);
            });
    };
    const toggleSwitchS = async () => {
        setDisable(true);
        const newState = !saklarS;
        console.log(item.id, saklarR ? 'on' : 'off', newState ? 'on' : 'off', saklarT ? 'on' : 'off', token);
        await SetSaklar(item.id, saklarR ? 'on' : 'off', newState ? 'on' : 'off', saklarT ? 'on' : 'off', token)
            .then(async function (response) {
                setDisable(false);
                setSaklarS(newState);
                await dispatch(getMcps(token));
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf !',
                    text2: 'On Off Lampu Gagal',
                    position: 'top',
                    visibilityTime: 5000,
                });
                console.log(error);
            });
    };
    const toggleSwitchT = async () => {
        setDisable(true);
        const newState = !saklarT;
        console.log(item.id, saklarR ? 'on' : 'off', saklarS ? 'on' : 'off', newState ? 'on' : 'off', token);
        await SetSaklar(item.id, saklarR ? 'on' : 'off', saklarS ? 'on' : 'off', newState ? 'on' : 'off', token)
            .then(async function (response) {
                setDisable(false);
                setSaklarT(newState);
                await dispatch(getMcps(token));
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                Toast.show({
                    type: 'error',
                    text1: 'Maaf !',
                    text2: 'On Off Lampu Gagal',
                    position: 'top',
                    visibilityTime: 5000,
                });
                console.log(error);
            });
    };

    const textValueR = saklarR ? 'MATIKAN' : 'NYALAKAN';
    const textValueS = saklarS ? 'MATIKAN' : 'NYALAKAN';
    const textValueT = saklarT ? 'MATIKAN' : 'NYALAKAN';

    useEffect(() => {
        const seting = () => {
            if (item.data_saklar.saklar_r === 'on') {
                setSaklarR(true);
            } else {
                setSaklarR(false);
            }
            if (item.data_saklar.saklar_s === 'on') {
                setSaklarS(true);
            } else {
                setSaklarS(false);
            }
            if (item.data_saklar.saklar_t === 'on') {
                setSaklarT(true);
            } else {
                setSaklarT(false);
            }
        };
        console.log('data mcp : ', item);
        //console.log('token', token);

        seting();

    }, []);

    return (
        <View
            style={{
                backgroundColor: '#ffffff',
                borderRadius: 10,
                padding: 10,
                borderColor: '#00ffff',
                borderWidth: 1,
                elevation: 1,
                marginHorizontal: 10,
            }}
        >
            <Loader loading={disable} />
            <View
                style={{ alignItems: 'center' }}
            >
                <Text
                    style={{
                        fontSize: 17,
                        color: '#4d4d66',
                        marginBottom: 10,
                    }}
                >Daya Terpakai</Text>
                <CircularProgress
                    value={item.data_sensor.total_watt}
                    maxValue={item.data_sensor.energy}
                    radius={100}
                    duration={1000}
                    progressValueColor={'#0073e6'}
                    titleFontSize={20}
                    title={'Watt'}
                    titleColor={'#0073e6'}
                    titleStyle={{ fontWeight: 'bold', marginTop: -20 }}
                    circleBackgroundColor={'transparent'}
                    activeStrokeColor={'#0073e6'}
                    activeStrokeSecondaryColor={'#00ffff'}
                    inActiveStrokeColor={'#00ffff'}
                    inActiveStrokeOpacity={0.3}
                    inActiveStrokeWidth={25}
                    activeStrokeWidth={25}
                />
            </View>

            {
                item.data_sensor.energy === 0 && item.data_sensor.freq === 0 ?
                    <View
                        style={{
                            backgroundColor: '#ffd280',
                            marginTop: 20,
                            borderRadius: 2,
                            padding: 10,
                            borderLeftWidth: 5,
                            borderLeftColor: '#ffa500',
                        }}
                    >
                        <Text
                            style={{ color: '#666666', fontSize: 15 }}
                        >MCP Tidak Tersambung Dengan Listrik</Text>
                    </View>
                    : null

            }

            <View
                style={{
                    flexDirection: 'row',
                    marginTop: 20,
                }}
            >
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Image source={require('../Assets/images/location.png')} style={{ width: 30, height: 30, tintColor: '#00ffff' }} />
                    <Text
                        style={{
                            fontSize: 15,
                            color: '#4d4d66',
                        }}
                    >
                        Lokasi
                    </Text>
                    <Text
                        style={{
                            fontSize: 15,
                            color: '#8e8e93',
                        }}
                    >
                        {item.lokasi}
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Image source={require('../Assets/images/electricity.png')} style={{ width: 30, height: 30, tintColor: '#00ffff' }} />
                    <Text
                        style={{
                            fontSize: 15,
                            color: '#4d4d66',
                        }}
                    >
                        Total Energi
                    </Text>
                    <Text
                        style={{
                            fontSize: 15,
                            color: '#8e8e93',
                        }}
                    >
                        {item.data_sensor.energy} KWh
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Image source={require('../Assets/images/pulse.png')} style={{ width: 30, height: 30, tintColor: '#00ffff' }} />
                    <Text
                        style={{
                            fontSize: 15,
                            color: '#4d4d66',
                        }}
                    >
                        Frekuensi
                    </Text>
                    <Text
                        style={{
                            fontSize: 15,
                            color: '#8e8e93',
                        }}
                    >
                        {item.data_sensor.freq} Hz
                    </Text>
                </View>
            </View>
            <View style={{
                flexDirection: 'row',
                marginTop: 10,
            }}>
                <View style={{
                    flex: 1,
                    backgroundColor: '#00ffff',
                    padding: 1,
                    borderRadius: 5,
                }}>
                    <TouchableOpacity
                        onPress={toggleSwitchR}
                    //disabled={disable}
                    >
                        <View
                            style={{
                                alignItems: 'center',
                                backgroundColor: saklarR ? '#0073e6' : '#00ffff',
                                paddingVertical: 10,
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: saklarR ? '#ffffff' : '#666666',
                                }}
                            >Fasa R</Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginTop: 15,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarR ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.watt_r} Watt</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginBottom: 15,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarR ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.volt_r} V, </Text>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarR ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.amp_r} A</Text>
                            </View>
                            {
                                saklarR ? (
                                    <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffff33' }} />
                                ) : (
                                    <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffffff' }} />
                                )
                            }
                            <View style={{ marginTop: 10 }}>
                                <Switch
                                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                                    thumbColor={saklarR ? '#ffff33' : '#ffffff'}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={toggleSwitchR}
                                    value={saklarR}
                                //disabled={disable}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{ backgroundColor: '#ffffff', width: 5 }} />

                <View style={{
                    flex: 1,
                    backgroundColor: '#00ffff',
                    padding: 1,
                    borderRadius: 5,
                }}>
                    <TouchableOpacity
                        onPress={toggleSwitchS}
                    >
                        <View
                            style={{
                                alignItems: 'center',
                                backgroundColor: saklarS ? '#0073e6' : '#00ffff',
                                paddingVertical: 10,
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: saklarS ? '#ffffff' : '#666666',
                                }}
                            >Fasa S</Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginTop: 15,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarS ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.watt_s} Watt</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginBottom: 15,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarS ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.volt_s} V, </Text>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarS ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.amp_s} A</Text>
                            </View>
                            {
                                saklarS ? (
                                    <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffff33' }} />
                                ) : (
                                    <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffffff' }} />
                                )
                            }
                            <View style={{ marginTop: 10 }}>
                                <Switch
                                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                                    thumbColor={saklarS ? '#ffff33' : '#ffffff'}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={toggleSwitchS}
                                    value={saklarS}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{ backgroundColor: '#ffffff', width: 5 }} />
                <View style={{
                    flex: 1,
                    backgroundColor: '#00ffff',
                    padding: 1,
                    borderRadius: 5,
                }}>
                    <TouchableOpacity
                        onPress={toggleSwitchT}
                    >

                        <View
                            style={{
                                alignItems: 'center',
                                backgroundColor: saklarT ? '#0073e6' : '#00ffff',
                                paddingVertical: 10,
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 15,
                                    color: saklarT ? '#ffffff' : '#666666',
                                }}
                            >Fasa T</Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginTop: 15,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarT ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.watt_t} Watt</Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginBottom: 15,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarT ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.volt_t} V, </Text>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: saklarT ? '#ffffff' : '#666666',
                                    }}
                                >{item.data_sensor.amp_t} A</Text>
                            </View>
                            {
                                saklarT ? (
                                    <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffff33' }} />
                                ) : (
                                    <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffffff' }} />
                                )
                            }
                            <View style={{ marginTop: 10 }}>
                                <Switch
                                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                                    thumbColor={saklarT ? '#ffff33' : '#ffffff'}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={toggleSwitchT}
                                    value={saklarT}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default Detail;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        marginTop: 10,
    },
    itemStyle: {
        fontSize: 20,
        color: '#333333',
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    descriptionContainer: {
        flexDirection: 'row',
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    textDescription: {
        marginLeft: 10,
        color: 'gray',
    },
});
