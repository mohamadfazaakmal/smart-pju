/* eslint-disable prettier/prettier */
import { combineReducers } from '@reduxjs/toolkit';
import loginReducer from '../Actions/Login';
import lokasiReducer from '../Actions/Locations';
import mcpReducer from '../Actions/Mcp';
import notifReducer from '../Actions/Notifikasi';
import energiReducer from '../Actions/Energi';
import pingReducer from '../Actions/Ping';
import userReducer from '../Actions/User';

export const Reducer = combineReducers({
    login: loginReducer,
    lokasi: lokasiReducer,
    mcp: mcpReducer,
    notif: notifReducer,
    energibymcp: energiReducer,
    ping: pingReducer,
    user: userReducer,
});
