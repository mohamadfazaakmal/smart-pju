/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getNotif = createAsyncThunk('/mcps/getNotif', async (AuthStr) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.get(`${baseUrl}/api/notifications`, { headers: { Authorization: 'Bearer ' + AuthStr } });
    return response.data.data;
});

const notifEntity = createEntityAdapter({
    selectId: (notif) => notif.id,
    //sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const notifSlice = createSlice({
    name: 'notif',
    initialState: notifEntity.getInitialState(),
    extraReducers: {
        [getNotif.fulfilled]: (state, actions) => {
            notifEntity.setAll(state, actions.payload);
        },
    },
    reducers: {},
});

export const notifSelector = notifEntity.getSelectors((state) => state.notif);
export default notifSlice.reducer;
