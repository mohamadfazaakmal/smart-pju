/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { Api } from '../../../Services/Api';

const PutNotif = async (id, token) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    //const token = useSelector(loginSelector);
    //const auth = 'Bearer ' + token;
    //console.log(auth);
    const apiURL = axios
        .put(`${baseUrl}/api/notifications/${id}`, { headers: { Authorization: 'Bearer ' + token } });
    // console.log(apiURL);
    return apiURL;
};

export default PutNotif;
