/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getUser = createAsyncThunk('/user/getUser', async (posts) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.get(`${baseUrl}/api/users/${posts.id}`, { headers: { Authorization: 'Bearer ' + posts.AuthStr } });
    return response?.data?.data;
});

// const notifEntity = createEntityAdapter({
//     selectId: (user) => user.accessToken,
//     sortComparer: (a, b) => a.title.localeCompare(b.title),
// });

const userSlice = createSlice({
    name: 'user',
    initialState: {
        data: {},
    },
    extraReducers: {
        [getUser.fulfilled]: (state, actions) => {
            state.data = actions.payload;
        },
    },
    reducers: {},
});

export const userSelector = (state) => state.user.data;
export default userSlice.reducer;
