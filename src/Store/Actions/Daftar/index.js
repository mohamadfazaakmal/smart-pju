/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { Api } from '../../../Services/Api';

export const postDaftar = async (posts) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.post(`${baseUrl}/api/register`, {
        nama: posts.nama,
        email: posts.email,
        password: posts.password,
        handphone: posts.telp,
        address: posts.alamat,
    })
        .then((respond) => {
            let res = respond.data;
            console.log(res);
            if (res.status) {
                return res;
            } else {
                return res;
            }
        })
        .catch((e) => {
            console.log('error', e);
            return e;
        });

    return response;
};
