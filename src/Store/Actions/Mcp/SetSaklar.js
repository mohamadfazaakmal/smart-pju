/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { Api } from '../../../Services/Api';

const SetSaklar = async (mcp, saklar_r, saklar_s, saklar_t, token) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    //const token = useSelector(loginSelector);
    //const auth = 'Bearer ' + token;
    //console.log(auth);
    const apiURL = axios
        .post(`${baseUrl}/api/mcps/setsaklar`, {
            mcp_id: mcp,
            saklar_r: saklar_r,
            saklar_s: saklar_s,
            saklar_t: saklar_t,
        }, { headers: { 'Content-Type': 'application/json', Authorization: 'Bearer ' + token } });
    // console.log(apiURL);
    return apiURL;
};

export default SetSaklar;
