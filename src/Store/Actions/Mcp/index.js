/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getMcps = createAsyncThunk('/mcps/getMcps', async (AuthStr) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.get(`${baseUrl}/api/mcps`, { headers: { Authorization: 'Bearer ' + AuthStr } });
    return response.data.data;
});

const notifEntity = createEntityAdapter({
    selectId: (mcp) => mcp.id,
    //sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const mcpSlice = createSlice({
    name: 'mcp',
    initialState: notifEntity.getInitialState(),
    extraReducers: {
        [getMcps.fulfilled]: (state, actions) => {
            notifEntity.setAll(state, actions.payload);
        },
    },
    reducers: {},
});

export const mcpSelector = notifEntity.getSelectors(state => state.mcp);
export default mcpSlice.reducer;
