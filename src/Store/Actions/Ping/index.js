/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getPing = createAsyncThunk('/ping/getPing', async (url, thunkAPI) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    //console.log(baseUrl);
    const response = await axios.post(`${url}/api/ping`, {
        url: url,
    })
        .then((respond) => {
            let res = respond?.data;
            console.log('ping : ', res);
            console.log('url', url);
            if (res.status) {
                storeData(url);
                return url;
            } else {
                return thunkAPI.rejectWithValue(res);
            }
        })
        .catch((e) => {
            console.log('error', e);
            return thunkAPI.rejectWithValue(e.message);
        });

    return response;
});

const storeData = async (value) => {
    try {
        await AsyncStorage.setItem('ping', value);
    } catch (e) {
        console.log(e);
    }
};

const pingSlice = createSlice({
    name: 'ping',
    initialState: {
        data: {},
    },
    reducers: {},
    extraReducers: {
        [getPing.fulfilled]: (state, actions) => {
            state.data = actions.payload;
        },
    },
});

export const pingSelector = (state) => state.ping.data;
export default pingSlice.reducer;
