/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getEnergiByMcp = createAsyncThunk('/energi/getEnergiByMcp', async (get, thunkAPI) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.get(`${baseUrl}/api/chart/per_jam?mcp_id=${get.id}`, { headers: { Authorization: 'Bearer ' + get.AuthStr } })
        .then((respond) => {
            let res = respond.data;
            console.log(res);
            if (res.status) {
                return res.data;
            } else {
                return thunkAPI.rejectWithValue(res);
            }
        })
        .catch((e) => {
            console.log('error', e);
            return thunkAPI.rejectWithValue(e.message);
        });
    return response;
});


const energiSlice = createSlice({
    name: 'energibymcp',
    initialState: {
        data: [],
    },
    [getEnergiByMcp.fulfilled]: (state, actions) => {
        state.data = actions.payload.data;
    },
    reducers: {},
});

export const energiSelector = (state) => state.energibymcp;
export default energiSlice.reducer;
