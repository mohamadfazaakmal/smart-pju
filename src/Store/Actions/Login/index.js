/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getLogin = createAsyncThunk('/login/getLogin', async (posts, thunkAPI) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.post(`${baseUrl}/api/login`, {
        email: posts.email,
        password: posts.password,
        fcmToken: posts.fcm,
    })
        .then((respond) => {
            let res = respond.data;
            console.log(res);
            if (res.status) {
                storeData(JSON.stringify(res.data));
                return res.data;
            } else {
                return thunkAPI.rejectWithValue(res);
            }
        })
        .catch((e) => {
            console.log('error', e);
            return thunkAPI.rejectWithValue(e.message);
        });

    return response;
});

const storeData = async (value) => {
    try {
        await AsyncStorage.setItem('token', value);
    } catch (e) {
        console.log(e);
    }
};

const loginSlice = createSlice({
    name: 'login',
    initialState: {
        isFetching: true,
        isLogin: false,
        isError: false,
        data: {},
    },
    reducers: {},
    extraReducers: {
        [getLogin.fulfilled]: (state, actions) => {
            state.data = actions.payload.data;
        },
    },
});

export const loginSelector = (state) => state.login;
export default loginSlice.reducer;
