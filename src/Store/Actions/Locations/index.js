/* eslint-disable prettier/prettier */
import { createSlice, createAsyncThunk, createEntityAdapter } from '@reduxjs/toolkit';
import { Api } from '../../../Services/Api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getLocations = createAsyncThunk('/locations/getLocations', async (AuthStr) => {
    const baseUrl = await AsyncStorage.getItem('ping');
    const response = await axios.get(`${baseUrl}/api/locations`, { headers: { Authorization: 'Bearer ' + AuthStr } });
    return response.data.data;
});

const notifEntity = createEntityAdapter({
    selectId: (lokasi) => lokasi.id,
});

const mapsSlice = createSlice({
    name: 'lokasi',
    initialState: notifEntity.getInitialState(),
    extraReducers: {
        [getLocations.fulfilled]: (state, actions) => {
            notifEntity.setAll(state, actions.payload);
        },
    },
    reducers: {},
});

export const lokasiSelector = notifEntity.getSelectors(state => state.lokasi);
export default mapsSlice.reducer;
