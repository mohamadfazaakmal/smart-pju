/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import { Text, TouchableOpacity } from 'react-native';
import React from 'react';

export default function CustomButton({ label, onPress }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: '#ffffff',
        padding: 15,
        borderRadius: 10,
        marginBottom: 30,
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontWeight: '700',
          fontSize: 20,
          color: '#000000',
        }}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}
