/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useEffect, useState } from 'react';
//import SetSaklar from '../middleware/SetSaklar';
import AsyncStorage from '@react-native-async-storage/async-storage';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import * as Icons from 'react-native-heroicons/solid';

//import { navigationRef } from '../Navigators/utils';
import { useDispatch, useSelector } from 'react-redux';
import loginSelector from '../Store/Actions/Login';
//import { getMcps } from '../Store/Control/controlSlice';

import BulbSVG from '../Assets/images/misc/bulb.svg';

const CustomFlatList = ({ item }) => {
    const [saklarR, setSaklarR] = useState(false);
    const [saklarS, setSaklarS] = useState(false);
    const [saklarT, setSaklarT] = useState(false);

    const [disable, setDisable] = useState(false);
    //const [auth, setAuth] = useState('');
    const dispatch = useDispatch();

    //const auth = useSelector(loginSelector);

    // const tombolR = async () => {
    //     const newState = !saklarR;
    //     setSaklarR(newState);
    //     //console.log(item.id, newState ? 'on' : 'off', saklarS ? 'on' : 'off', saklarT ? 'on' : 'off', auth.accessToken);
    //     await SetSaklar(item.id, newState ? 'on' : 'off', saklarS ? 'on' : 'off', saklarT ? 'on' : 'off', auth.accessToken)
    //         .then(async function (response) {
    //             await dispatch(getMcps(auth.accessToken));
    //             console.log(JSON.stringify(response.data));
    //             // if (response) {
    //             //     await setDATA(response.data);
    //             // } else {
    //             //     console.log('Data Tidak Ada');
    //             // }
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //         });
    // };

    // const tombolS = async () => {
    //     const newState = !saklarS;
    //     setSaklarS(newState);
    //     //console.log(item.id, saklarR ? 'on' : 'off', newState ? 'on' : 'off', saklarT ? 'on' : 'off', auth.accessToken);
    //     await SetSaklar(item.id, saklarR ? 'on' : 'off', newState ? 'on' : 'off', saklarT ? 'on' : 'off', auth.accessToken)
    //         .then(async function (response) {
    //             await dispatch(getMcps(auth.accessToken));
    //             console.log(JSON.stringify(response.data));
    //             // if (response) {
    //             //     await setDATA(response.data);
    //             // } else {
    //             //     console.log('Data Tidak Ada');
    //             // }
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //         });
    // };

    // const tombolT = async () => {
    //     const newState = !saklarT;
    //     setSaklarT(newState);
    //     //console.log(item.id, saklarR ? 'on' : 'off', saklarS ? 'on' : 'off', newState ? 'on' : 'off', auth.accessToken);
    //     await SetSaklar(item.id, saklarR ? 'on' : 'off', saklarS ? 'on' : 'off', newState ? 'on' : 'off', auth.accessToken)
    //         .then(async function (response) {
    //             await dispatch(getMcps(auth.accessToken));
    //             console.log(JSON.stringify(response.data));
    //             // if (response) {
    //             //     await setDATA(response.data);
    //             // } else {
    //             //     console.log('Data Tidak Ada');
    //             // }
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //         });
    // };

    const detail = async () => {
        //navigationRef.navigate('Detail', { id: item.id });
    };

    const data = {
        mcp_id: item.id,
        saklar_r: saklarR ? 'on' : 'off',
        saklar_s: saklarS ? 'on' : 'off',
        saklar_t: saklarT ? 'on' : 'off',
    };

    // const getToken = async () => {
    //     try {
    //         const jsonValue = await AsyncStorage.getItem('accessToken');
    //         console.log('GetDataTokenBaru', String(jsonValue));
    //         setAuth(String(jsonValue));
    //         return jsonValue != null ? JSON.parse(jsonValue) : null;
    //     } catch (e) {
    //         // error reading value
    //     }
    // };

    const textValueR = saklarR ? 'MATIKAN' : 'NYALAKAN';
    const textValueS = saklarS ? 'MATIKAN' : 'NYALAKAN';
    const textValueT = saklarT ? 'MATIKAN' : 'NYALAKAN';

    useEffect(() => {
        const seting = () => {
            if (item.data_saklar.saklar_r === 'on') {
                setSaklarR(true);
            } else {
                setSaklarR(false);
            }
            if (item.data_saklar.saklar_s === 'on') {
                setSaklarS(true);
            } else {
                setSaklarS(false);
            }
            if (item.data_saklar.saklar_t === 'on') {
                setSaklarT(true);
            } else {
                setSaklarT(false);
            }
        };

        seting();

    }, []);

    useEffect(() => {
        const disabled = () => {
            item.status ? setDisable(false) : setDisable(true);
        };

        disabled();

    }, []);

    return (
        <>
            <View style={style.container}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flex: 1 }}>
                        <Text style={style.roundText}>{item.name}</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        {
                            item.status ? (
                                <View
                                    style={{
                                        height: 20,
                                        width: 20,
                                        borderRadius: 10,
                                        backgroundColor: '#33ff33',
                                        elevation: 2,
                                    }}
                                />
                            ) : (
                                <View
                                    style={{
                                        height: 20,
                                        width: 20,
                                        borderRadius: 10,
                                        backgroundColor: '#ff0000',
                                        elevation: 2,
                                    }}
                                />
                            )
                        }
                    </View>
                </View>
                <View style={{ backgroundColor: '#ffffff', height: 3, marginVertical: 5 }} />
                <View
                    style={{
                        height: 100,
                        backgroundColor: '#4aa8ff',
                        elevation: 2,
                        borderRadius: 5,
                        padding: 5,
                    }}
                >
                    <Text>ssdsfs</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 1 }}>
                        {
                            saklarR ? (
                                <>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffff33' }} />
                                    </View>
                                </>
                            ) : (
                                <>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffffff' }} />
                                    </View>
                                </>
                            )
                        }
                    </View>

                    <View style={{ flex: 1 }}>
                        {
                            saklarS ? (
                                <>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffff33' }} />
                                    </View>
                                </>
                            ) : (
                                <>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffffff' }} />
                                    </View>
                                </>
                            )
                        }
                    </View>

                    <View style={{ flex: 1 }}>
                        {
                            saklarT ? (
                                <>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffff33' }} />
                                    </View>
                                </>
                            ) : (
                                <>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image source={require('../Assets/images/bulb.png')} style={{ width: 50, height: 50, tintColor: '#ffffff' }} />
                                    </View>
                                </>
                            )
                        }
                    </View>
                </View>
                <View style={{ backgroundColor: '#ffffff', height: 5, marginVertical: 10 }} />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={detail}
                        style={style.tombolDetail}>
                        <Text style={style.textD}>Lihat Detail</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    );
};

export default CustomFlatList;

const style = StyleSheet.create({
    container: {
        maxHeight: 500,
        backgroundColor: '#007aff',
        margin: 5,
        padding: 10,
        borderRadius: 5,
    },
    voltText: {
        fontSize: 16, color: '#ffffff', marginTop: 10, marginBottom: 5,
    },
    roundImage: {
        height: 30,
        width: 30,
    },
    tombolOn: {
        height: 30,
        backgroundColor: '#ffffff',//ff0000
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        elevation: 10,
        margin: 5,
    },
    tombolOff: {
        height: 30,
        backgroundColor: '#ffffff',//07e124
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        elevation: 10,
        margin: 5,
    },
    tombolDetail: {
        height: 30,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#ffffff',
        borderWidth: 2,
    },
    roundCallout: {
        height: 100,
        width: 150,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    roundText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#ffffff',
    },
    texthead: {
        fontSize: 18,
        color: '#ffffff',
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#0d73f0',
    },
    textD: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#ffffff',
    },
});
