/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, StatusBar, StyleSheet, Alert, Animated } from 'react-native';
import * as Animatable from 'react-native-animatable';
import * as Icons from 'react-native-heroicons/outline';
import * as Icons2 from 'react-native-heroicons/solid';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector } from 'react-redux';
import { notifSelector } from '../Store/Actions/Notifikasi';
import { Menu, MenuOption, MenuOptions, MenuTrigger } from 'react-native-popup-menu';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { navigationRef } from '../Navigations/Utils';
import Toast from 'react-native-toast-message';

const Header = ({ navigation, tinggi, judul }) => {

    const dataNotif = useSelector(notifSelector.selectAll);
    const totalNotif = dataNotif.filter((val) => val.isUnRead === true);

    const keluar = async () => {
        try {
            //const setup = '192.168.5.111';
            await AsyncStorage.removeItem('token');
            //await AsyncStorage.removeItem('url');
            navigation.navigate('Auth');
            Toast.show({
                type: 'success',
                text1: 'Berhasil',
                text2: 'Anda telah logout!',
                position: 'top',
                visibilityTime: 5000,
            });
        } catch (e) {
            Toast.show({
                type: 'error',
                text1: 'Maaf',
                text2: 'Logout gagal, silahkan coba lagi!',
                position: 'top',
                visibilityTime: 5000,
            });
        }
    };

    return (
        <Animatable.View animation="fadeInDownBig" style={{ height: tinggi }}>
            <LinearGradient
                animation="fadeInUpBig"
                colors={['#2F80ED', '#346eff', '#36D1DC']}
                style={{ ...StyleSheet.absoluteFillObject, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}
                useAngle={true}
                angle={150}
                angleCenter={{ x: 0.5, y: 0.5 }}
            >
                <View style={{ paddingHorizontal: 20, paddingTop: StatusBar.currentHeight, flex: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 2 }}>
                        <View>
                            <Text style={{ fontSize: 25, fontWeight: '500', color: '#ffffff' }}>
                                {judul}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 2 }}>
                            <View style={{ marginRight: 10 }}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Notifikasi')}
                                >
                                    <Icons.BellIcon size={30} color={'#ffffff'} />
                                    {
                                        totalNotif.length !== 0 ?
                                            <View
                                                style={{
                                                    position: 'absolute',
                                                    backgroundColor: 'red',
                                                    width: 16,
                                                    height: 16,
                                                    borderRadius: 8,
                                                    right: 0,
                                                    top: 0,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}>
                                                <Text
                                                    style={{
                                                        color: 'white',
                                                        fontSize: 11,
                                                    }}>
                                                    {totalNotif.length}
                                                </Text>
                                            </View>
                                            : null
                                    }
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Menu>
                                    <MenuTrigger>
                                        <Icons.DotsVerticalIcon size={30} color={'#ffffff'} />
                                    </MenuTrigger>
                                    <MenuOptions style={{ justifyContent: 'center' }}>
                                        <MenuOption onSelect={() => navigation.navigate('Profil')} >
                                            <View
                                                style={{ flexDirection: 'row', alignItems: 'center' }}
                                            >
                                                <Icons.UserIcon size={25} color={'#666666'} />
                                                <Text style={{ color: '#666666', marginLeft: 5 }}>Profil</Text>
                                            </View>
                                        </MenuOption>
                                        <MenuOption onSelect={() => navigation.navigate('Informasi')} >
                                            <View
                                                style={{ flexDirection: 'row', alignItems: 'center' }}
                                            >
                                                <Icons.InformationCircleIcon size={25} color={'#666666'} />
                                                <Text style={{ color: '#666666', marginLeft: 5 }}>Tentang Aplikasi</Text>
                                            </View>
                                        </MenuOption>
                                        <MenuOption onSelect={() => {
                                            Animated.timing(keluar(), {
                                                duration: 300,
                                                useNativeDriver: true,
                                            });
                                        }} >
                                            <View
                                                style={{ flexDirection: 'row', alignItems: 'center' }}
                                            >
                                                <Icons.LogoutIcon size={25} color={'#666666'} />
                                                <Text style={{ color: '#666666', marginLeft: 5 }}>Logout</Text>
                                            </View>
                                        </MenuOption>
                                    </MenuOptions>
                                </Menu>
                            </View>
                        </View>
                    </View>
                </View>
            </LinearGradient>
        </Animatable.View>
    );
};

export default Header;
