/* eslint-disable prettier/prettier */
import React from 'react';
import {
    StyleSheet,
    View,
    Modal,
    ActivityIndicator,
    Text,
} from 'react-native';

const Loader = ({ loading }) => {

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={loading}
            onRequestClose={() => { console.log('close modal'); }}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <View style={{ height: 5 }} />
                    <ActivityIndicator
                        animating={loading}
                        size={'large'}
                        color={'#0d73f0'}
                    />
                    <Text style={{ color: '#666666', fontSize: 13 }}>
                        Loading...
                    </Text>
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000030',
    },
    activityIndicatorWrapper: {
        backgroundColor: '#ffffff',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
});

export default Loader;
