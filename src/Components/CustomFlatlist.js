/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useEffect, useState } from 'react';
//import SetSaklar from '../middleware/SetSaklar';
import AsyncStorage from '@react-native-async-storage/async-storage';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import * as Icons from 'react-native-heroicons/solid';

//import { navigationRef } from '../Navigators/utils';
import { useDispatch, useSelector } from 'react-redux';
import loginSelector from '../Store/Actions/Login';
//import { getMcps } from '../Store/Control/controlSlice';

import BulbSVG from '../Assets/images/misc/bulb.svg';
import { navigationRef } from '../Navigations/Utils';

const CustomFlatList = ({ item }) => {
    const [saklarR, setSaklarR] = useState(false);
    const [saklarS, setSaklarS] = useState(false);
    const [saklarT, setSaklarT] = useState(false);

    const [disable, setDisable] = useState(false);
    //const [auth, setAuth] = useState('');
    const dispatch = useDispatch();


    const detail = async () => {
        navigationRef.navigate('Detail', { id: item.id, pilihan: 'DETAIL' });
    };

    const data = {
        mcp_id: item.id,
        saklar_r: saklarR ? 'on' : 'off',
        saklar_s: saklarS ? 'on' : 'off',
        saklar_t: saklarT ? 'on' : 'off',
    };

    useEffect(() => {
        const seting = () => {
            if (item.data_saklar.saklar_r === 'on') {
                setSaklarR(true);
            } else {
                setSaklarR(false);
            }
            if (item.data_saklar.saklar_s === 'on') {
                setSaklarS(true);
            } else {
                setSaklarS(false);
            }
            if (item.data_saklar.saklar_t === 'on') {
                setSaklarT(true);
            } else {
                setSaklarT(false);
            }
        };

        seting();

    }, []);

    useEffect(() => {
        const disabled = () => {
            item.status ? setDisable(false) : setDisable(true);
        };

        disabled();

    }, []);

    return (
        <>
            {
                item.status ?
                    <View style={[style.container, { borderLeftWidth: 5, borderLeftColor: '#33ff33' }]}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={style.roundText}>{item.name}</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <TouchableOpacity
                                    onPress={detail}
                                    style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{
                                        fontSize: 15,
                                        color: '#2F80ED',
                                    }}>Lihat Detail</Text>
                                    <Icons.ChevronRightIcon size={20} color={'#2F80ED'} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginTop: 5,
                            }}
                        >
                            <View
                                style={{
                                    flex: 1,
                                }}
                            >
                                <View
                                    style={{
                                        justifyContent: 'center',
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 17,
                                            color: '#00004d',
                                        }}
                                    >
                                        Fasa R
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginTop: 5,
                                    }}
                                >
                                    <View
                                        style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <Image source={require('../Assets/images/electricity.png')} style={{ width: 25, height: 25, tintColor: '#00004d' }} />
                                    </View>
                                    <View
                                        style={{
                                            flex: 2,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontSize: 20,
                                                color: '#33ffd6',
                                                marginRight: 5,
                                                fontWeight: '700',
                                            }}
                                        >{
                                                item.status ? item.data_sensor.watt_r : 0
                                            }</Text>
                                        <Text
                                            style={{
                                                fontSize: 15,
                                                color: '#666666',
                                            }}
                                        >Watt</Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        marginTop: 5,
                                    }}
                                >
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                        }}
                                    >
                                        {
                                            item.data_saklar.saklar_r === 'on' ?
                                                <Image source={require('../Assets/images/switch-on.png')} style={{ width: 35, height: 35 }} />
                                                :
                                                <Image source={require('../Assets/images/switch-off.png')} style={{ width: 35, height: 35 }} />
                                        }
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    flex: 1,
                                }}
                            >
                                <View
                                    style={{
                                        justifyContent: 'center',
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 17,
                                            color: '#00004d',
                                        }}
                                    >
                                        Fasa S
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginTop: 5,
                                    }}
                                >
                                    <View
                                        style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <Image source={require('../Assets/images/electricity.png')} style={{ width: 25, height: 25, tintColor: '#00004d' }} />
                                    </View>
                                    <View
                                        style={{
                                            flex: 2,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontSize: 20,
                                                color: '#33ffd6',
                                                marginRight: 5,
                                                fontWeight: '700',
                                            }}
                                        >{
                                                item.status ? item.data_sensor.watt_s : 0
                                            }</Text>
                                        <Text
                                            style={{
                                                fontSize: 15,
                                                color: '#666666',
                                            }}
                                        >Watt</Text>
                                    </View>
                                </View>

                                <View
                                    style={{
                                        marginTop: 5,
                                    }}
                                >
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                        }}
                                    >
                                        {
                                            item.data_saklar.saklar_s === 'on' ?
                                                <Image source={require('../Assets/images/switch-on.png')} style={{ width: 35, height: 35 }} />
                                                :
                                                <Image source={require('../Assets/images/switch-off.png')} style={{ width: 35, height: 35 }} />
                                        }
                                    </View>
                                </View>
                            </View>
                            <View
                                style={{
                                    flex: 1,
                                }}
                            >
                                <View
                                    style={{
                                        justifyContent: 'center',
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 17,
                                            color: '#00004d',
                                        }}
                                    >
                                        Fasa T
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        marginTop: 5,
                                    }}
                                >
                                    <View
                                        style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <Image source={require('../Assets/images/electricity.png')} style={{ width: 25, height: 25, tintColor: '#00004d' }} />
                                    </View>
                                    <View
                                        style={{
                                            flex: 2,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Text
                                            style={{
                                                fontSize: 20,
                                                color: '#33ffd6',
                                                marginRight: 5,
                                                fontWeight: '700',
                                            }}
                                        >{
                                                item.status ? item.data_sensor.watt_t : 0
                                            }</Text>
                                        <Text
                                            style={{
                                                fontSize: 15,
                                                color: '#666666',
                                            }}
                                        >Watt</Text>
                                    </View>
                                </View>
                                <View
                                    style={{
                                        marginTop: 5,
                                    }}
                                >
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                        }}
                                    >
                                        {
                                            item.data_saklar.saklar_t === 'on' ?
                                                <Image source={require('../Assets/images/switch-on.png')} style={{ width: 35, height: 35 }} />
                                                :
                                                <Image source={require('../Assets/images/switch-off.png')} style={{ width: 35, height: 35 }} />
                                        }
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    :
                    <View style={[style.container, { borderLeftWidth: 5, borderLeftColor: '#ff0000' }]}>
                        <View style={{ marginVertical: 5 }}>
                            <View style={{ flex: 1 }}>
                                <Text style={style.roundText}>{item.name} Tidak Ada Koneksi !</Text>
                            </View>
                        </View>
                    </View>
            }
        </>
    );
};

export default CustomFlatList;

const style = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        marginHorizontal: 5,
        marginTop: 5,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderRadius: 5,
        elevation: 2,
    },
    voltText: {
        fontSize: 16, color: '#00004d', marginTop: 10, marginBottom: 5,
    },
    roundImage: {
        height: 30,
        width: 30,
    },
    tombolOn: {
        height: 30,
        backgroundColor: '#00004d',//ff0000
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        elevation: 10,
        margin: 5,
    },
    tombolOff: {
        height: 30,
        backgroundColor: '#00004d',//07e124
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        elevation: 10,
        margin: 5,
    },
    tombolDetail: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: '#00d3e0',
        paddingHorizontal: 15,
        paddingVertical: 8,
    },
    roundCallout: {
        height: 100,
        width: 150,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    roundText: {
        fontSize: 20,
        fontWeight: '500',
        color: '#00004d',
    },
    texthead: {
        fontSize: 18,
        color: '#00004d',
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#0d73f0',
    },
    textD: {
        fontWeight: '500',
        fontSize: 15,
        color: '#00004d',
    },
});
